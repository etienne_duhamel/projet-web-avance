# Gestion de Stocks

Pour lancer le site web : 

 - Cloner ce dépôt
 - Dans le dossier Gestion_Stocks, lancer `npm run devstart`
 - Avec un nouveau terminal, depuis le dossier react-front, lancer `npm install react-scripts` puis `npm start`
 - `localhost:3000` depuis un navigateur (Firefox / Chrome de préférence)
