import axios from "axios";
const headers = {
  "Content-Type": "application/json"
};
const burl = "http://localhost:4000";

export default {
  login: function(email, password) {
    return axios.post(
      `${burl}/users/login`,
      {
        email,
        password
        
      },
      {
        headers: headers
      }
    );
  },
  signup: function(send) {
    return axios.post(`${burl}/users/signup`, send, { headers: headers });
  },

  updateUser: function(send) {
    return axios.put(`${burl}/users/update/`+localStorage.getItem("token"), send, { headers: headers});
  },

  editRole: function(send) {
    return axios.put(`${burl}/users/updateid/`+send.id, send, { headers: headers});
  },

  isAuth: function() {
    return localStorage.getItem("token") !== null;
  },

  logout: function() {
    localStorage.clear();
  },

  fetchUser: function() {
    return fetch(`${burl}/users/user/`+localStorage.getItem("token"));
  },

  deleteUser: function(send) {
    return axios.post(`${burl}/users/delete/`+send);
  },

  fetchArticles: function() {
    return fetch(`${burl}/articles`);
  },

  deleteArticle: function(send) {
    return axios.post(`${burl}/articles/delete/`+send);
  },

  addArticle: function(send) {
    return axios.post(`${burl}/articles/newarticle/`, send, { headers: headers });;
  },

  giveOpinion: function(send) {
    return axios.post(`${burl}/opinion/giveopinion`, send, { headers: headers });
  },

  fetchOpinions: function() {
    return fetch(`${burl}/opinion/opinionlist`);
  },

  changepwd: function(send) {
    return axios.post(`${burl}/users/changepwd/`+localStorage.getItem("token"), send, { headers: headers});
  },


  user_list: function() {
    
    return fetch(`${burl}/users/userlist`);
  }, 

  user_detail(send) {
    return fetch(`${burl}/users/userfile/`+send);
  },

  client_list: function() {
    
    return fetch(`${burl}/representing/allClients/`+localStorage.getItem("username"));
  }, 

  myclient_list: function() {
    return fetch(`${burl}/representing/myClients/`+localStorage.getItem("username"))
  },

  addclient: function(send) {
    return axios.post(`${burl}/representing/addClient/`, send, { headers: headers})
  },

  deleteClient: function(send) {
    return axios.post(`${burl}/representing/deleteClient`, send, { headers: headers });
  },

  addCommand: function(send) {
    return axios.post(`${burl}/addOrder/addcommand`, send, { headers: headers });
  },

  command_list: function() {
    return fetch(`${burl}/addOrder/commande-list`);
  }, 

  command_detail: function(send) {
    return fetch(`${burl}/addOrder/details/`+send);
  },

  myorders: function(e) {
    return fetch(`${burl}/addOrder/myorders/`+e);
  },

  client_orders: function(send) {
    return fetch(`${burl}/addOrder/clientorders/`+send);
  },

  deleteOrder: function(send) {
    return axios.post(`${burl}/addOrder/delete/`+send);
  },

  userInfo: function (datas) {
    if (datas){
      
      var rcv = datas.items.data;
      if (rcv){
        rcv.map(function(item) {
          localStorage.setItem("username", item.username);
          localStorage.setItem("role", item.role);
          localStorage.setItem("email", item.email);
          localStorage.setItem("date", item.createdAt);
          localStorage.setItem("address_facturation", JSON.stringify(item.address_facturation));
        });
      }
    
    }
  }
};