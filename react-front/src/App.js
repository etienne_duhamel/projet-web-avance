/*
import React, { Component } from "react";
import injectTapEventPlugin from "react-tap-event-plugin";
// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941

import "./App.css";
import Loginscreen from "./Loginscreentest";
//injectTapEventPlugin();
*/
import React, { Component } from "react";
//import { Route } from "react-router-dom";
import { Switch } from "react-router-dom";
import { Dashboard } from "./components/Dashboards/Dashboard.jsx";
import { Vitrine } from "./components/vitrine/Vitrine.jsx";
import { Login } from "./components/Login/Login.jsx";
import { addOrder } from "./components/addOrder/addOrder.jsx";
import { MyAccount } from "./components/MyAccount/MyAccount.jsx"
import { UserList } from "./components/User_list/User_list.jsx"
import { CommandList } from "./components/Command_list/Command_list.jsx";
import { EditInfo } from "./components/MyAccount/EditInfo"
import { Signup } from "./components/Signup/Signup.jsx";
import { ChangePwd } from "./components/MyAccount/Change_password.jsx";
import { SubmitOpinion } from "./components/Opinions/SubmitOpinions";
import { Opinion_List } from "./components/Opinions/Opinions";
import { myOrdersList } from "./components/myOrders/orders_list";
import { ArticlesList } from "./components/Articles/Articles_list";
import { AddArticle } from "./components/Articles/Add_Article";
import { ClientList } from "./components/Representing/Client_list.jsx";
import { MyClientList } from "./components/Representing/myClients.jsx";
import Details from "./components/Command_list/details.jsx";
import Userdetails from "./components/User_list/user_details.jsx";
import { EditRole } from "./components/User_list/edit_role.jsx";
import { ClientOrderList } from "./components/Representing/ClientOrderList.jsx";
import { PrivateRoute, RestrictedRoute, AdminRepreRoute, AdminRoute, RepresentantRoute, UserRoute } from "./components/PrivateRoute.jsx";

import "./App.css";





class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-content">
          <Switch>
            <RestrictedRoute exact path="/" component={Vitrine} />
            <RestrictedRoute exact path="/login" component={Login} />
            <RestrictedRoute exact path="/signup" component={Signup} />
            <PrivateRoute path="/dashboard" component={Dashboard} />
            <PrivateRoute path="/addOrder" component={addOrder} />
            <PrivateRoute path="/myaccount" component={MyAccount} />
            <PrivateRoute path="/editinfo" component={EditInfo} />
            <PrivateRoute path="/changepwd" component={ChangePwd} />
            <PrivateRoute path="/opinionlist" component={Opinion_List} />
            <PrivateRoute path="/myorders" component={myOrdersList} />
            <AdminRoute path="/userlist" component={UserList} />
            <AdminRoute path="/stock" component={ArticlesList} />
            <AdminRepreRoute path="/userfile" component={Userdetails} />
            <AdminRoute path="/addarticle" component={AddArticle} />
            <PrivateRoute path="/commandlist" component={CommandList} />
            <PrivateRoute path="/details" component={Details} />
            <PrivateRoute path="/giveopinion" component={SubmitOpinion} />
            <AdminRoute path="/editrole" component={EditRole} />
            <RepresentantRoute path="/clientorders" component={ClientOrderList} />
            <RepresentantRoute path="/clientlist" component={ClientList} />
            <RepresentantRoute path="/myclients" component={MyClientList} />
            
          </Switch>
        </div>
      </div>
    );
  }
}
export default App;

