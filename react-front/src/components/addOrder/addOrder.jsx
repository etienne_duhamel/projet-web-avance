import React, { Component } from "react";

import API from "../../utils/API";
import Select from "react-select";
import countryList from 'react-select-country-list';

import Navbar from "../Navbars/Navbar";
const options = countryList().getData();

export class addOrder extends Component {

  state = {
    isLoading: true,
    items: [],
    error: null,
    selectedOption: null,
    articles: [],
    rowIndex: 0,
    rows: [],
    total: 0,
    country: "",
    shipping_address: "",
    city: "",
    postal: "",
  }
  handleChangeInTab = idx => e => {
    const {value} = e.target;
    const rows = [...this.state.rows];
    rows[idx].quantity=value;
    this.setState({
      rows
    });
    console.log("QUANTITY : ",this.state.rows[idx].quantity);
    this.priceUpdate(e);
  };
  handleAddRow = (e) => {
    console.log("ADDING ROW");
    e.preventDefault();
    const items = {
      texte: "",
      price: 0,
      quantity: 1
    };
    var found =0;
    //var drop = document.getElementById("articles-select");
    if(this.state.selectedOption != null){
    console.log("IF 1");
    items.texte = this.state.selectedOption.label;
    items.price = this.state.selectedOption.value;
    console.log("END IF 1",items.texte, items.price, items.quantity);
    for(var i=0; i < this.state.rows.length ; i++){
      if(this.state.rows[i].texte === items.texte){
        found +=1;
      }
    }
    if (found === 0){
     this.setState({
        rows: [...this.state.rows, items]
       });
       this.state.rows.push(items);
       console.log("END ADD : ", this.state.rows);
      this.priceUpdate(e);
      }    
    }
  };
  
  handleRemoveSpecificRow = idx => e => {
    e.preventDefault();
    const rows = [...this.state.rows];
    rows.splice(idx, 1);
    this.setState({ rows });
    this.state.rows=rows;
    this.priceUpdate(e);
  };

  handleChange = selectedOption => {
    this.setState({ selectedOption });
    console.log(`Option selected:`, selectedOption);
  };

  handleChangeCountry = country => {
    this.setState({ country });
    console.log(`country:`, country);
  };

  priceUpdate= e => {
    console.log("PRICE UPDATE");
    e.preventDefault();
    var total =0;
    for(var i=0; i < this.state.rows.length ; i++){
      total += this.state.rows[i].price * this.state.rows[i].quantity;
    }
    
    console.log("ROWS : ", this.state.rows);
    this.state.total=total;
    
  };

  back = (e) => {
    window.history.back();
  };

  articles_list = () => {

    API.fetchArticles()
      .then((response) => {
        return response.json();
      })
      .then(data => {

        console.log(data);
        this.setState({
          items: data,
          isLoading: false
        });
        console.log(this.state.items.articles.length);

        const articles = [];
        for (var i = 0; i < this.state.items.articles.length; i++) {
          this.state.articles.push({ 'value': this.state.items.articles[i].Prix, 'label': this.state.items.articles[i].Name })
        }
        console.log("ARTICLES : ", this.state.articles);

      }
      )
      .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.articles_list();
  }

  getArticles(data) {
    data.map(function (item) {

    })

  }

  onSubmit = e => {
    e.preventDefault();
    var cmd = [];
    cmd.push([this.state.country,this.state.city,this.state.postal,this.state.shipping_address]);
    const newCommand = {
    username: localStorage.getItem("username"),
    total: this.state.total,
    shipping_address: cmd,
    facturation_address: JSON.parse(localStorage.getItem("address_facturation")),
    rows: this.state.rows
    };
    console.log(newCommand.rows);
    this.send(newCommand);
    
  };

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  send = async (newCommand) => {
    
    try {
      console.log(newCommand)
      API.addCommand(newCommand)
      .then(()=> window.location ="/myorders")
      
      
    } catch (error) {
      console.error(error);
      
    }
    
  };

  render() {
    const { selectedOption, country } = this.state;
    return (
      <div>
        <style type="text/css" dangerouslySetInnerHTML={{ __html: "\n        .form-label-left {\n            width: 150px;\n        }\n\n        .form-line {\n            padding-top: 12px;\n            padding-bottom: 12px;\n        }\n\n        .form-label-right {\n            width: 150px;\n        }\n\n        body,\n        html {\n            margin: 0;\n            padding: 0;\n            background: #fff;\n        }\n\n        .form-all {\n            margin: 0px auto;\n            padding-top: 0px;\n            width: 690px;\n            color: #555 !important;\n            font-family: \"Lucida Grande\", \"Lucida Sans Unicode\", \"Lucida Sans\", Verdana, sans-serif;\n            font-size: 14px;\n        }\n\n        .form-radio-item label,\n        .form-checkbox-item label,\n        .form-grading-label,\n        .form-header {\n            color: false;\n        }\n    " }} />
        <style type="text/css" id="form-designer-style" dangerouslySetInnerHTML={{ __html: "\n        /* Injected CSS Code */\n        .form-label.form-label-auto {\n\n            display: block;\n            float: none;\n            text-align: left;\n            width: 100%;\n\n        }\n\n        /* Injected CSS Code */\n    " }} />

        <Navbar></Navbar>

        <form noValidate onSubmit={this.onSubmit} className="add-commande-form"  acceptCharset="utf-8" autoComplete="on">
          <div role="main" className="form-all" style={{ marginTop: 50 }}>
            <ul className="form-section page-section">
              <li id="cid_1" className="form-input-wide" data-type="control_head">
                <div className="form-header-group  header-large">
                  <div className="header-text httal htvam"><br />
                    <h1 id="header_1" className="form-header" data-component="header">
                      New Order
                          </h1>
                  </div>
                </div>
              </li>

              <li className="form-line">
                <label className="form-label form-label-top"> Catalogue </label>
                <div className="form-input-wide">
                  <div data-wrapper-react="true">
                    <Select name="articles-select" id="articles-select" menuPlacement="auto"
                      placeholder="Choose an article"
                      isSearchable value={selectedOption}
                      onChange={this.handleChange}
                      options={this.state.articles}></Select>
                    <br />
                    <button onClick={event => {this.handleAddRow(event)}}>Add Product</button>
                    <br /><br />
                    <table style={{ width: '100%' }}>
                      <thead><tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th>Price</th>
                      </tr>
                      </thead>

                      {/* TABLE EDIT */}


                      <tbody>
                        {this.state.rows.map((item, idx) => (
                          <tr id="addr0" key={idx}>
                            <td>{this.state.rows[idx].texte}
                            </td>
                            <td>
                              <input
                              id={this.state.rows[idx].texte}
                              name={this.state.rows[idx].texte}
                                type="number"
                                value={this.state.rows[idx].quantity}
                                min="1"
                                onChange={this.handleChangeInTab(idx)}
                                className="form-control"
                              />
                            </td>
                            <td><span>{this.state.rows[idx].quantity*this.state.rows[idx].price}</span></td>
                            <td>
                              <button
                                className="btn btn-outline-danger btn-sm"
                                onClick={this.handleRemoveSpecificRow(idx)}
                              >
                                Remove
                        </button>
                            </td>
                          </tr>
                        ))}
                      </tbody>



                      {/* END TABLE EDIT */}



                    </table>

                    {/*   */}
                    <br />
                    <span className="form-payment-total">
                      <b>
                        <span id="total">
                          Total : {this.state.total || 0}€
                              </span>
                        <span className="form-payment-price">
                          <span data-wrapper-react="true">
                            <span id="payment_total">
                            </span>
                          </span>
                        </span>
                      </b>
                    </span>
                  </div>
                </div>
              </li>
              <li className="form-line" data-type="control_address" id="id_10">
                <br /><br />
                <div id="cid_10" className="form-input-wide">
                  <table summary className="form-address-table" align="center">
                    <tbody>
                      <tr><th style={{ textAlign: "center" }}> Shipping Address
                      </th></tr>
                      <tr><td colSpan={2}>
                        <span style={{ verticalAlign: 'top' }}>
                          <label htmlFor="select_country">Country</label>
                          <Select id="select_country" 
                            menuPlacement="auto" 
                            placeholder="Choose a country" 
                            isSearchable options={options}
                            isSearchable value = {country}
                            onChange={this.handleChangeCountry}>
                          </Select>
                        </span>
                      </td>
                      </tr>
                      <tr>
                        <td colSpan={2}>
                          <span style={{ verticalAlign: 'top' }}>
                            <label htmlFor="shipping_address" id="shipping_address">Shipping Address </label>
                            <input 
                              id="shipping_address" 
                              onChange={this.onChange}
                              value={this.state.shipping_address} 
                              name="shipping_address"/>

                          </span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td>
                        
                        <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="city">City</label>
                            <input 
                              id="city" 
                              onChange={this.onChange}
                              value={this.state.city} 
                              name="city"/>

                          </span>
                        </td>
                        <td>
                        <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="postal">Postal Code</label>
                            <input 
                              id="postal" 
                              onChange={this.onChange}
                              value={this.state.postal} 
                              name="postal"/>

                          </span>
                        
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </li>
              <li className="form-line" data-type="control_button" id="id_13">
                <div id="cid_13" className="form-input-wide">
                  <div style={{ textAlign: 'center' }} data-align="center" className="form-buttons-wrapper paypal-submit-container">
                    <div className="paypal-submit-button-wrapper">
                    </div>
                    <button type="submit" className="form-submit-button" value="Submit" name="subButton">
                      Submit Order
                          </button>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </form>
        <button onClick={event => {this.back(event)}}>Go Back</button>
      </div>
    );
  }
}





  