import React from 'react';
import Address from "../Address/address";


export default class PrintmyorderList extends React.Component {
   
    ship = () => {
        window.location="/addorder";
    }
    back = (e) => {
        window.history.back();
    }

    
    
render = () =>{
    
    const {datas, whenClicked} = this.props;
    console.log("PROPS", datas);
    console.log(datas.length)
    if(!datas){

        return(<h1>Loading...</h1>)
    }
    else if(datas.length == "0"){

        return(
            <div>
                <br/>
                <br/>
                <h2>You don't have orders yet, let's ship!</h2>
                <button onClick={this.ship}>Go Shipping</button>
                <button onClick={event => {this.back(event)}}>Go Back</button>

               
            </div>
        )
    }

    else {
    return(
        

        <div>
        <br/>
        <br/>
        <button onClick={event => {this.back(event)}}>Go Back</button>
        <br/>
        <br/>
        {datas.map(function(item,index){
            
            return ([
            <div> 
                <table>

                    <tr>
                        <th>Order ID</th>
                        <th>Shipping Address</th>
                        <th>Facturation Address</th>
                        <th>Date</th>
                    </tr>
                    
                    <tr id={index+1}>
                    
                        <td key={item._id}>{item._id}</td>
                        <td><Address datas={item.shipping_address}></Address></td>
                        <td><Address datas={item.user.address_facturation}></Address></td>
                        <td key={item.date_commande}>{item.date_commande}</td>
                        <button type="button" name="cancel" id={index+1} value={item._id} onClick={whenClicked}>Cancel Order</button>
                        <button type="button" name="details" id={index+1} value={item._id} onClick={whenClicked}>Details</button>
                    </tr>
                </table>
                <h4>Order Summary</h4>
                <table>

                    {item.commande.map(function(prod,index2){
                        console.log(prod)
                        return ([
                            <table>
                                <tr>
                                    <th>Article n°{index2}</th>
                                    <th>Price/Unit</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                </tr>
                                <tr>
                                    <td>{prod[0]}</td>
                                    <td>{prod[1]}€</td>
                                    <td>{prod[2]}</td>
                                    <td>{prod[3]}€</td>
                                </tr>
                                </table>
                            ])
                    
                    })}

            <td key={item.total}><strong>Total = {item.total}€</strong></td>
            
            </table>
            <br/>
            <br/>
            <br/>
        </div>
        
            ])
        })}  
        
            </div>
        )

    }

}


}
