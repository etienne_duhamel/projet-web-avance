import React from "react";
import API from "../utils/API.js";
import { Route, Redirect } from "react-router-dom";



export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (API.isAuth() === false) {
        return <Redirect to="/" />;
      } else {
        return <Component {...props} />;
      }
    }}
  />
);

export const RestrictedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (API.isAuth() === true) {
        return <Redirect to="/dashboard" />;
      } else {
        return <Component {...props} />;
      }
    }}
  />
);

export const AdminRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (API.isAuth() === true && localStorage.getItem("role") === "Admin") {
        console.log(localStorage.getItem("role"));
        return <Component {...props} />;
      } else {
        return <Redirect to="/dashboard" />;
      }
    }}
  />
);

export const AdminRepreRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (API.isAuth() === true && ( localStorage.getItem("role") === "Admin" || localStorage.getItem("role") === "Representant")) {
        console.log(localStorage.getItem("role"));
        return <Component {...props} />;
      } else {
        return <Redirect to="/dashboard" />;
      }
    }}
  />
);
export const RepresentantRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (API.isAuth() === true && localStorage.getItem("role") === "Representant") {
        console.log(localStorage.getItem("role"));
        return <Component {...props} />;
      } else {
        return <Redirect to="/dashboard" />;
      }
    }}
  />
);

export const UserRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      if (API.isAuth() === true && localStorage.getItem("role") === "User") {
        console.log(localStorage.getItem("role"));
        return <Component {...props} />;
      } else {
        return <Redirect to="/dashboard" />;
      }
    }}
  />
);
