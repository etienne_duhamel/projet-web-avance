import React from 'react';
import Address from "../Address/address";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { PdfDocument } from "./bill_pdf";


export default class PrintDetails extends React.Component {
   
redirection = () => {

    window.history.back();
   
   
}

render = () =>{
    
    const datas = this.props;
    console.log("PROPS", datas);
    const child=datas.datas;
    if(!child){

        return(<h1>Loading...</h1>)
    }
    else if(datas.length === 0){

        return(
            <div>
                <br/>
                <br/>
                <h2>Error on Reading data...Please Retry or report to your Administrator</h2>
               
            </div>
        )
    }

    else {
    return(
        

        <div>
        <br/>
        <br/>
        <br/>
       
            <div> 
                <table>

                    <tr>
                        <th>Order ID</th>
                        <th>Shipping Address</th>
                        <th>Facturation Address</th>
                        <th>Date</th>
                        <th>Invoice</th>
                    </tr>
                    
                    <tr>
                    
                        <td key={child._id}>{child._id}</td>
                        <td><Address datas={child.shipping_address}></Address></td>
                        <td><Address datas={child.user.address_facturation}></Address></td>
                        <td key={child.date_commande}>{child.date_commande}</td>
                        <td>
                        <PDFDownloadLink
                            document={<PdfDocument data={child} />}
                            fileName="bill.pdf"
                            style={{
                                textDecoration: "none",
                                padding: "10px",
                                color: "#4a4a4a",
                                backgroundColor: "#f2f2f2",
                                border: "1px solid #4a4a4a"
                                }}
                            >
                            {({ blob, url, loading, error }) =>
                            loading ? "Loading document..." : "Download Pdf"
                            }
                        </PDFDownloadLink>
                        </td>
                       
                    </tr>
                </table>
                <h4>Order Summary</h4>
                <table>

                    {child.commande.map(function(prod,index2){
                        console.log(prod)
                        return ([
                            <table>
                                <tr>
                                    <th>Article n°{index2}</th>
                                    <th>Price/Unit</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                </tr>
                                <tr>
                                    <td>{prod[0]}</td>
                                    <td>{prod[1]}€</td>
                                    <td>{prod[2]}</td>
                                    <td>{prod[3]}€</td>
                                </tr>
                                </table>
                            ])
                    
                    })}

            <td key={child.total}><strong>Total = {child.total}€</strong></td>
            </table>
            <br/>
            <br/>
            <button onClick={this.redirection}>Back</button>
        </div>
        
       
        
            </div>
        )

    }

}


}