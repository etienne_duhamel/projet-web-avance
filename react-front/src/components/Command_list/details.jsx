import React from 'react';
import API from "../../utils/API";
import Navbar from "../Navbars/Navbar";
import PrintDetails from './Print_details';


export default class Details extends React.Component {
   
    state = {
        isLoading: true,
        items: [],
        error: null
    }
    
    command_details = () => {
        
        const id = window.location.search.substring(4)
       
        console.log(id)
        
        API.command_detail(id)
            .then((response) => {
                return response.json();
            })
            .then(data => {
                
                //console.log(data);
                
                this.setState({
                    items: data,
                    isLoading: false
                });

                console.log(this.state.items.data);
                
            }
            )
            // Catch any errors we hit and update the app
            .catch(error => this.setState({ error, isLoading: false }));
            
    }

    

    componentDidMount() {
        this.command_details();
      }


      render() {
        var rcv = this.state.items.data;
        console.log("MAIN :",rcv);
        if(!rcv){
            return (
                <div>
                    <Navbar></Navbar> 
                    <br />
                    <br />
                    <br />
                    <h1>Loading...</h1>
                    </div>)
        }
        return (
            <div>
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                <Navbar></Navbar> 
                <br />
                <br />
                <br />
                <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>Details</strong></span></h1>
                <PrintDetails datas={rcv}></PrintDetails>                
               
            </div>
          );
        
    }
}


