import React, { Fragment } from "react";
import {
    Page,
    Text,
    View,
    Document,
    StyleSheet,
    Image
} from "@react-pdf/renderer";

import Logo from "../vitrine/images/Logo.png";
import tampon from "./images/tampon.png";

const borderColor = '#90e5fc'

const styles = StyleSheet.create({
    page: {
        backgroundColor: "#ffffff"
    },
    section: {
        margin: 10,
        padding: 10,
        flexGrow: 1
    },
    Title: {
        marginTop: 25,
        fontSize: 30,
        textAlign: "center"
    },
   
    image: {
        height: 200,
        width: 595,
        alignItems: "right"
    },

    tampon: {
        marginTop: 45,
        marginLeft: 400,
        height: 80,
        width: 150,
    },

    tableContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 24,
        borderWidth: 1,
        borderColor: '#bff0fd',
    },
    container: {
        flexDirection: 'row',
        borderBottomColor: '#bff0fd',
        backgroundColor: '#bff0fd',
        borderBottomWidth: 1,
        alignItems: 'center',
        height: 24,
        textAlign: 'center',
        fontStyle: 'bold',
        
    },
    description: {
        width: '40%',
        borderRightColor: borderColor,
        borderRightWidth: 1,
    },
    qty: {
        width: '20%',
        borderRightColor: borderColor,
        borderRightWidth: 1,
        textAlign: 'center',
    },
    rate: {
        width: '20%',
        borderRightColor: borderColor,
        borderRightWidth: 1,
        textAlign: 'center',
    },
    amount: {
        width: '20%',
        textAlign: 'center',
    },
    invoiceNoContainer: {
        flexDirection: 'row',
        marginTop: 36,
        justifyContent: 'flex-end',
        fontSize: 10,
        marginLeft: -5
    },
    invoiceDateContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        fontSize: 12,
        marginLeft: -5
    },
    invoiceDate: {
        fontSize: 12,
        fontStyle: 'bold',
    },
    label: {
        width: 80,
        fontSize: 15,
        fontStyle: 'bold'
    },
    headerContainer: {
        marginTop: 20,
        fontSize: 15,
        marginLeft: 12

    },
    billTo: {
        marginTop: 20,
        paddingBottom: 3,
        fontSize: 15,
        fontStyle: 'bold',
        fontFamily: 'Helvetica-Oblique'
    },
    row: {
        flexDirection: 'row',
        borderBottomColor: '#bff0fd',
        borderBottomWidth: 1,
        alignItems: 'center',
        height: 24,
        fontStyle: 'bold',
    },
    
    description_total: {
        width: '85%',
        textAlign: 'right',
        borderRightColor: borderColor,
        borderRightWidth: 1,
        paddingRight: 8,
    },
    total: {
        width: '15%',
        textAlign: 'center',
        paddingRight: 8,
    },
    titleContainer:{
        flexDirection: 'row',
        marginTop: 20
    },
    reportTitle:{
        fontSize: 12,
        textAlign: 'center',
        textTransform: 'uppercase',
}

});
const tableRowsCount = 11;


export function PdfDocument(props) {
    console.log("pdf props", props.data);
    return (
        <Document>
            <Page style={styles.page}>
                
                <Image
                    style={styles.image}
                    source={Logo}
                />
                <Text style={styles.Title}>Invoice</Text>
                <Fragment>
                    <View style={styles.invoiceNoContainer}>
                        <Text style={styles.label}>Order No: </Text>
                        <Text style={styles.invoiceDate}>{props.data._id}</Text>
                    </View >
                    <View style={styles.invoiceDateContainer}>
                        <Text style={styles.label}>Date: </Text>
                        <Text style={styles.invoiceDate}>{props.data.date_commande}</Text>
                    </View >
                </Fragment>
                <View style={styles.headerContainer}>
                    <Text style={styles.billTo}>Bill To:</Text>
                    <Text>{props.data.user.username}</Text>
                    <Text>{props.data.user.email}</Text>
                    <Text>{props.data.shipping_address.Address}</Text>
                    <Text>{props.data.shipping_address.Postal} {props.data.shipping_address.City} • {props.data.shipping_address.Country}</Text>
                </View>
                <View style={styles.tableContainer}>
                </View>
                <View style={styles.container}>
                    <Text style={styles.description}>Article</Text>
                    <Text style={styles.rate}>€/Unit</Text>
                    <Text style={styles.qty}>Quantity</Text>
                    <Text style={styles.amount}>Price</Text>
                </View>
                {props.data.commande.map(function(art){
                        
                        return (
                            <View style={styles.row}>
                                <Text style={styles.description}>{art[0]}</Text>
                                <Text style={styles.rate}>{art[1]}</Text>
                                <Text style={styles.qty}>{art[2]}</Text>
                                <Text style={styles.amount}>{art[3]}€</Text>
                            </View>
                        )
                    
                    })}
                <View style={styles.row}>
                    <Text style={styles.description_total}>TOTAL</Text>
                    <Text style={styles.total}>{props.data.total}€</Text>
                </View>
                <View style={styles.titleContainer}>
                    <Text style={styles.reportTitle}>Thank you for your business ©GestionStocks</Text>
                </View>
                <Image
                    style={styles.tampon}
                    source={tampon}
                />                 
            </Page>
        </Document>
    );
}
