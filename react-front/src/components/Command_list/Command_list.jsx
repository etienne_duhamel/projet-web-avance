import React, { Component } from "react";

import API from "../../utils/API";
import Navbar from "../Navbars/Navbar";
import PrintCommandList from "./PrintCommand_List"


export class CommandList extends Component {

    state = {
        isLoading: true,
        items: [],
        error: null
    }
    
    command_list = () => {
        
        API.command_list()
            .then((response) => {
                return response.json();
            })
            .then(data => {
                
                //console.log(data);
                this.setState({
                    items: data,
                    isLoading: false
                });
                console.log(this.state.items.data);
            }
            )
            // Catch any errors we hit and update the app
            .catch(error => this.setState({ error, isLoading: false }));
    }

    handleClick = (e) => {
        if(e.target.name === "delete"){
            console.log("delete")
            API.deleteOrder(e.target.value)
           
            window.location.reload()
        }
        else if(e.target.name === "details") {
            console.log("details")
            window.location ='/details?id='+e.target.value
        }
        
            
        
    }

    componentDidMount() {
        this.command_list();
      }


      render() {
        var rcv = this.state.items.data;
        console.log("MAIN :",rcv);
        if(!rcv){
            return (
                <div>
                    <Navbar></Navbar> 
                    <br />
                    <br />
                    <br />
                    <h1>Loading...</h1>
                    </div>)
        }
        return (
            <div>
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                <Navbar></Navbar> 
                <br />
                <br />
                <br />
                <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>Command List</strong></span></h1>
                <PrintCommandList datas={rcv} whenClicked={(event)=>{this.handleClick(event)}}></PrintCommandList>


                
                
               
                
                
            </div>
          );
        
    }
}
//<PrintmyorderList datas={rcv}></PrintmyorderList>