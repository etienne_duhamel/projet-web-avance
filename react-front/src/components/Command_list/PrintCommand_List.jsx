import React from 'react';

export default class PrintCommandList extends React.Component {
   
    ship = () => {
        window.location="/addorder";
    }

    back = (e) => {
        window.history.back();
    }


render = () =>{
    
    const {datas, whenClicked} = this.props;

    if(!datas){

        return(<h1>Loading...</h1>)
    }
    else if(datas.length == "0"){

        return(
            <div>
                <br/>
                <br/>
                <h2>You don't have orders yet, let's ship!</h2>
                <button onClick={this.ship}>Go Shipping</button>
                <button onClick={event => {this.back(event)}}>Go Back</button>
            </div>
        )
    }
    return(
        <div>
            <br/>
            <br/>
            <button onClick={event => {this.back(event)}}>Go Back</button>
            <br/>
            <table>

                <tr>
                    <th>Order ID</th>
                    <th>Content</th>
                    <th>Total</th>
                    <th>Date</th>
                </tr>

        {datas.map(function(item,index){
            
            return ([
            
            <tr>
                <td key={item._id}>{item._id}</td>
                <table>
                    <tr>
                        <th>Article</th>
                        <th>Quantity</th>

                    </tr>
                {item.commande.map(function(prod){
                   return ([
                    
                    <tr>
                        <td>{prod[0]}</td>
                        <td>{prod[2]}</td>

                    </tr>
                   
                        
                    ])
                   
                })}
                 </table>
                <td key={item.total}>{item.total}€</td>
                <td key={item.date_commande}>{item.date_commande}</td>
                <button type="button" name="delete" id={index+1} value={item._id} onClick={whenClicked}>Delete</button>
                <button type="button" name="details" id={index+1} value={item._id} onClick={whenClicked}>Details</button>
                
            </tr>
            ])
        })}

            </table>
            </div>
        )
    }
}






        