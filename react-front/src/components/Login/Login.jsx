import React, { Component } from "react";
import { Link } from "react-router-dom";
import API from "../../utils/API";

export class Login extends Component {
  state = {
      email: "",
      password: "",
      errors: {},
      isLoading: true,
      items: [],
      error: null
  
    }
  

  disconnect = () => {
    API.logout();
    window.location = "/";
  };

  fetchUsers = () => {
    API.fetchUser()
    
  .then((response) => {
    return response.json();
  })
  .then(data => {
    
    this.setState({
      items: data,
      isLoading: false
    });
    
    console.log(this.state.items);
    API.userInfo(this.state);
    console.log("retour du userinfo");
    this.redirection(localStorage.getItem("role"));
    
    
  }
  )
  // Catch any errors we hit and update the app
  .catch(error => this.setState({ error, isLoading: false }));
  }
  
  redirection = (role) => {
    
    
    if(role === "Admin"){
      window.location = "/";
    }
    else if (role === "Representant"){
      window.location = "/";
    }
    else {
      window.location = "/";
    }
    
  };
  
  send = async () => {
    const { email, password } = this.state;
    if (!email || email.length === 0) {
      return;
    }
    if (!password || password.length === 0) {
      return;
    }
    try {
      const { data } = await API.login(email, password);
      localStorage.setItem("token", data.token);
     
      
      
      
    
    } catch (error) {
      console.error(error);
      alert('Invalid credentials');
      window.location ="/login";
    }
    
    this.fetchUsers();
    
    
    
    
  };
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const userData = {
    email: this.state.email,
    password: this.state.password
    
    };
    console.log(userData);
    this.send(userData);
    
  };
  render() {
    const { errors } = this.state;
    return (
      <div className="container">
        <nav className="z-depth-0">
          <div className="nav-wrapper white">
            <Link
              to="/"
              style={{
                fontFamily: "monospace"
              }}
              className="col s5 brand-logo center black-text"
            >
              <i className="material-icons">code</i>
              Gestion Stocks
            </Link>
          </div>
        </nav>
        <div style={{ marginTop: "4rem" }} className="row">
          <div className="col s8 offset-s2">
            <Link to="/" className="btn-flat waves-effect">
              <i className="material-icons left">keyboard_backspace</i> Back to
              home
            </Link>
            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
              <h4>
                <b>Login</b> below
              </h4>
              <p className="grey-text text-darken-1">
                Don't have an account? <Link to="/signup">Sign up</Link>
              </p>
            </div>
            <form noValidate onSubmit={this.onSubmit}>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.email}
                  error={errors.email}
                  id="email"
                  type="email"
                />
                <label htmlFor="email">Email</label>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.password}
                  error={errors.password}
                  id="password"
                  type="password"
                />
                <label htmlFor="password">Password</label>
              </div>
              <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                <button
                  style={{
                    width: "150px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px",
                    marginTop: "1rem"
                  }}
                  type="submit"
                  className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                >
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}