import React from 'react';
import API from "../../utils/API";
import Navbar from "../Navbars/Navbar";
import PrintUserDetails from './PrintDetails';



export default class Userdetails extends React.Component {
   
    state = {
        isLoading: true,
        items: [],
        error: null
    }
    
    user_details = () => {
        
        const id = window.location.search.substring(4)
        console.log(id)
        
        API.user_detail(id)
            .then((response) => {
                return response.json();
            })
            .then(data => {
                
                //console.log(data);
                
                this.setState({
                    items: data,
                    isLoading: false
                });
                

                console.log(this.state.items.data);
                
            }
            )
            // Catch any errors we hit and update the app
            .catch(error => this.setState({ error, isLoading: false }));
            
    }

    

    componentDidMount() {
        this.user_details();
      }

      handleClick = (e) => {
        
            console.log("Edit Role")
            console.log(e.target.value)
            //window.location ='/editrole?id='+e.target.value
        }

      render() {
        var rcv = this.state.items.data;
        console.log("MAIN :",rcv);
        if(!rcv){
            return (
                <div>
                    <Navbar></Navbar> 
                    <br />
                    <br />
                    <br />
                    <h1>Loading...</h1>
                    </div>)
        }
        return (
            <div>
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                <Navbar></Navbar> 
                <br />
                <br />
                <br />
                <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>User File</strong></span></h1>
                <PrintUserDetails datas={rcv}  whenClicked={(event)=>{this.handleClick(event)}}></PrintUserDetails>
        
                
            </div>
          );
        
    }
}

