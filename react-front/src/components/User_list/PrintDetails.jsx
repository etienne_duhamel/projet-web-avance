import React from 'react';
import Address from "../Address/address";


export default class PrintUserDetails extends React.Component {
   

redirection = () => {
   
        window.history.back();
   
}
edit_role = () => {
    const id = window.location.search.substring(4)
    console.log(id)
    window.location='/editrole?id='+id
}

edit = () => {
    if (localStorage.getItem("role") === "Admin"){
                    
        return(
            <button onClick={this.edit_role}>Edit Role</button>
        )
    }
}
render = () =>{
    
    const datas = this.props;
    const whenClicked = this.props;
    console.log("PROPS", datas);
    const child=datas.datas;
    if(!child){

        return(<h1>Loading...</h1>)
    }
    else if(datas.length === 0){

        return(
            <div>
                <br/>
                <br/>
                <h2>Error on Reading data...Please Retry or report to your</h2>
               
            </div>
        )
    }

    else {
    return(
        

        <div>
        <br/>
        <br/>
        <br/>
       
            <div> 
                <table>

                    <tr>
                        <th>Client ID</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Facturation Address</th>
                        <th>Role</th>
                        <th>Created At</th>
                        
                    </tr>
                    
                    <tr>
                    
                        <td key={child._id}>{child._id}</td>
                        <td key={child.username}>{child.username}</td>
                        <td key={child.email}>{child.email}</td>
                        <td><Address datas={child.address_facturation}></Address></td>
                        <td key={child.role}>{child.role}</td>

                        <td key={child.createdAt}>{child.createdAt}</td>
                       
                    </tr>
                </table>
                <br/>
                <br/>
                {this.edit()}

            <button onClick={this.redirection}>Back</button>
        </div>
        
       
        
            </div>
        )

    }

}


}