import React from "react";
import API from "../../utils/API";
import Navbar from "../Navbars/Navbar"
import Select from "react-select";

const roles = [
    { value: "Admin", label: "Admin"},
    { value: "Representant", label: "Representant"},
    { value: "User", label: "User"},
  ]

export class EditRole extends React.Component {
  
   state = {
    id: window.location.search.substring(4),
    role: ""
  }
  
  save = async (updateRole) => {
    
    try{

      console.log(updateRole)
      await API.editRole(updateRole); //before, await return was stored in const{data}
      window.location="/userlist";
      
      
    } catch (error) {
      console.error(error);
    }
  };
 
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const updateRole = {
    id: this.state.id,
    role: this.state.role
    };
    console.log("Click");
    this.save(updateRole);
  };
  handleChangeRole = role => {
    this.setState({ role });
    console.log(`role:`, role);
  };
  redirection = () => {
    window.history.back();
  }
  
  render() {
    
    const { role } = this.state;
    return (
          <div>
            <Navbar></Navbar>
            <br/>
            <br/>
            <br/>
            <br/>
            <h2>Edit Role</h2>
            <br/>
            <br/>
            <button onClick={this.redirection}>Go Back</button>
            <div className="container">
              <div className="row">
                <div className="col s8 offset-s2">
                  <form noValidate onSubmit={this.onSubmit}>       
              <div className="input-field col s12">
                  <table summary className="form-address-table">
                    <tbody>
                      <tr><th> Role
                      </th></tr>
                      <tr><td colSpan={2}>
                        <span style={{ verticalAlign: 'top' }}>
                          <label htmlFor="choose a role">Role</label>
                          <Select id="Choose a role" 
                            menuPlacement="auto" 
                            placeholder={this.state.role}
                            isSearchable options={roles}
                            isSearchable value = {role}
                            onChange={this.handleChangeRole}>
                          </Select>
                        </span>
                      </td>
                      </tr>
                    </tbody>
                  </table>
                </div>






                    <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                      <button
                        style={{
                          width: "150px",
                          borderRadius: "3px",
                          letterSpacing: "1.5px",
                          marginTop: "1rem"
                        }}
                        type="submit"
                        className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                      >
                        SAVE
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
    );
  
  }
}