import React from 'react';

export default class PrintList extends React.Component {
    
    redirection = () => {
        window.history.back();
    }


render = () =>{
   
    const {datas, whenClicked} = this.props;
    console.log("PROPS", datas);
    if(!datas){

        return(<h1>Loading...</h1>)
    }
    return(
        <div>
            <br/>
            <br/>
            <br/>
            <table>

                <tr>
                    <th>User ID</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Options</th>
                </tr>


        {datas.map(function(item,index){
            
            return ([
            
            <tr id={index+1}>
                <td key={item._id}>{item._id}</td>
                <td key={item.username}>{item.username}</td>
                <td key={item.email}>{item.email}</td>
                <td key={item.role}>{item.role}</td>
                <td><button type="button" name="delete" id={index+1} value={item.username} onClick={whenClicked}>Delete User</button>
                <button type="button" name= "details" id={index+1} value={item._id} onClick={whenClicked}>Details</button></td>
            </tr>
            ])
        })}
            </table>
            <button onClick={this.redirection}>Back</button>

        </div>
        )
}


}




        