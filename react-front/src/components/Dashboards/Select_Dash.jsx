import React from "react";
import artifice from "./images/artifice.png"
import covid19 from "./images/covid19.png"


const SelectDashboard = () => {

  const donation = () => {
    alert("Thank you for your donation ❤");
    window.location = "/";
  };


  if(localStorage.getItem("role") === "Admin"){
     return([
      <div>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <style dangerouslySetInnerHTML={{__html: "\nbody {font-family: \"Lato\", sans-serif}\n.mySlides {display: none}\n" }} />
      <h1>Welcome Master!</h1>
      <h3>Can we do something for you?</h3>
      <br/>
      <div className="w3-half">
          <img src={artifice} alt="artifice" style={{width: '20%'}} className="w3-circle w3-hover-opacity" />
      </div>
      <div className="w3-half">
          <img src={artifice} alt="artifice" style={{width: '20%'}} className="w3-circle w3-hover-opacity" />
      </div>     
      <div className="w3-container w3-padding-64 w3-center" id="shipping">
        <h2><span className="w3-xlarge w3-border-teal w3-bottombar">ADMIN PANEL</span></h2>
          <br/>
          <br/>
          <br/>
          <br/>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3> MANAGE ORDERS</h3>
                <p>Overview</p>
                <a href="/commandlist">ALL ORDERS -></a>
              </div>
            </div>
          </div>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3>MANAGE USERS</h3>
                <p>Overview</p>
                <a href="/userlist">ALL USERS -></a>
              </div>
            </div>
          </div>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3>MANAGE PRODUCTS</h3>
                <p>Overview</p>
                <a href="/stock">ALL ARTICLES -></a>
              </div>
            </div>
          </div>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3>OPINIONS</h3>
                <p>Have a look to all opinions</p>
                <a href="/opinionlist">Opinions</a>
              </div>
            </div>
          </div>
       </div>
     </div>
  ])
  }
  else if(localStorage.getItem("role") === "Representant"){
      return([
        <div>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
            <style dangerouslySetInnerHTML={{__html: "\nbody {font-family: \"Lato\", sans-serif}\n.mySlides {display: none}\n" }} />
            <h1>Welcome Home {localStorage.getItem("username")}!</h1>
            <h3>We hope everything is good!!</h3>
            <br/>
            <div className="w3-half">
                <img src={artifice} alt="artifice" style={{width: '20%'}} className="w3-circle w3-hover-opacity" />
            </div>
            <div className="w3-half">
                <img src={artifice} alt="artifice" style={{width: '20%'}} className="w3-circle w3-hover-opacity" />
            </div>     
            <div className="w3-container w3-padding-64 w3-center" id="shipping">
              <h2><span className="w3-xlarge w3-border-teal w3-bottombar">START MANAGING</span></h2>
                <br/>
                <br/>
                <br/>
                <br/>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>SHIP</h3>
                      <p>let yourself be tempted by one of our products</p>
                      <a href="/addOrder">New Order -></a>
                    </div>
                  </div>
                </div>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>MANAGE</h3>
                      <p>Managing panel</p>
                      <a href="/myorders">My Orders -></a>
                      <br/> 
                      <a href="/myclients">My Clients -></a> 
                      <br/>
                      <a href="/clientlist">Add Clients -></a>
                    </div>
                  </div>
                </div>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>SETTINGS</h3>
                      <p>Manage your account</p>
                      <a href="/editinfo">Edit my profile -></a>
                    </div>
                  </div>
                </div>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>OPINIONS</h3>
                      <p>Feel free to give us your opinion</p>
                      <a href="/giveopinion">Give my opinion -></a>
                    </div>
                  </div>
                </div>
                <div className="w3-container w3-padding-64 w3-center" id="donate">
                <div className="w3-quarter">
                <br/>
                <br/>
                <br/>
                <br/>
                <img src={covid19} alt="covid19" style={{width: '30%'}} />
                </div>
                <div className="w3-quarter">
                <br/>
                <br/>
                <br/>
                <br/>
                  <p><strong>COVID-19 Solidarity Response Fund for WHO (World Health Organization)</strong></p> 
                </div>
                <div className="w3-quarter">
                <br/>
                <br/>
                <br/>
                <br/>
                <button onClick={donation}>Donate US25$</button>
                </div>
                </div>

             </div>
           </div>
  ])
  }
  else {
      return([
        <div>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
            <style dangerouslySetInnerHTML={{__html: "\nbody {font-family: \"Lato\", sans-serif}\n.mySlides {display: none}\n" }} />
            <h1>Welcome Home {localStorage.getItem("username")}!</h1>
            <h3>We hope everything is good!!</h3>
            <br/>
            <div className="w3-half">
                <img src={artifice} alt="Boss" style={{width: '20%'}} className="w3-circle w3-hover-opacity" />
            </div>
            <div className="w3-half">
                <img src={artifice} alt="Boss" style={{width: '20%'}} className="w3-circle w3-hover-opacity" />
            </div>     
            <div className="w3-container w3-padding-64 w3-center" id="shipping">
              <h2><span className="w3-xlarge w3-border-teal w3-bottombar">START SHIPPING</span></h2>
                <br/>
                <br/>
                <br/>
                <br/>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>SHIP</h3>
                      <p>let yourself be tempted by one of our products</p>
                      <a href="/addOrder">New Order -></a>
                    </div>
                  </div>
                </div>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>ORDERS</h3>
                      <p>Check your recents orders</p>
                      <a href="/myorders">My Orders -></a> 
                    </div>
                  </div>
                </div>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>SETTINGS</h3>
                      <p>Manage your account</p>
                      <a href="/editinfo">Edit my profile -></a>
                    </div>
                  </div>
                </div>
                <div className="w3-quarter">
                  <div className="w3-card w3-white">
                    <div className="w3-container">
                      <h3>OPINIONS</h3>
                      <p>Feel free to give us your opinion</p>
                      <a href="/giveopinion">Give my opinion -></a>
                    </div>
                  </div>
                </div>
                <br/>
                <br/>
                <div className="w3-quarter">
                <br/>
                <br/>
                <br/>
                <br/>
                <img src={covid19} alt="covid19" style={{width: '30%'}} />
                </div>
                <div className="w3-quarter">
                <br/>
                <br/>
                <br/>
                <br/>
                  <p><strong>COVID-19 Solidarity Response Fund for WHO (World Health Organization)</strong></p> 
                </div>
                <div className="w3-quarter">
                <br/>
                <br/>
                <br/>
                <br/>
                <button onClick={donation}>Donate US25$</button>
                </div>

             </div>
           </div>
  ])

  }
  
}
export default SelectDashboard;