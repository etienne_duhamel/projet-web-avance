import React from 'react';
import API from "../../utils/API";
import Address from "../Address/address";


const UserData = (datas) =>{
    
    API.userInfo(datas);

    if(!datas){
        return <h1>Loading...</h1>
    }
    else {
        
        const username = localStorage.getItem("username");
        const role = localStorage.getItem("role");
        const email = localStorage.getItem("email");
        const date = localStorage.getItem("date");
        const address_facturation = JSON.parse(localStorage.getItem("address_facturation"));
        console.log(address_facturation);

        return([
            
            <div>
                
                <table>
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Date</th>
                        <th>address Facturation</th>
                    </tr>
                    <tr>
                        <td>{username}</td>
                        <td>{email}</td>
                        <td>{role}</td>
                        <td>{date}</td>
                        <Address datas={address_facturation}></Address>
                    </tr>
                    
                </table>
            </div>
       
        ])
    }
}
export default UserData;

