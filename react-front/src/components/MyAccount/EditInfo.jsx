import React from "react";
import API from "../../utils/API";
import Navbar from "../Navbars/Navbar"
import Select from "react-select";
import countryList from 'react-select-country-list';

const options = countryList().getData();

export class EditInfo extends React.Component {
  
   state = {
      username: localStorage.getItem("username"),
      email: localStorage.getItem("email"),
      adresse_facturation: JSON.parse(localStorage.getItem("address_facturation")),
      country:'',
      city:'',
      address:'',
      postal:''
  }
  
  save = async (updateUser) => {
    
    const { username, email} = this.state;
    var address = [];
    if(this.state.address === ''){
      this.state.address=this.state.adresse_facturation.Address;
    }
    if(this.state.country === ''){
      this.state.country=this.state.adresse_facturation.Country;
    }
    if(this.state.city === ''){
      this.state.city=this.state.adresse_facturation.City;
    }
    if(this.state.postal === ''){
      this.state.postal=this.state.adresse_facturation.Postal;
    }
    address.push([this.state.country,this.state.city,this.state.postal,this.state.address]);

    const new_address=address[0]
    
    if (!email || email.length === 0) return alert("Email must be provided");
    try {
      
      await API.updateUser({username, email, new_address}); //before, await return was stored in const{data}
      window.location="/myaccount";
   
    } catch (error) {
      console.error(error);
    }
  };
 
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  back = (e) => {
    window.history.back();
  };
  onSubmit = e => {
    e.preventDefault();
    const updateUser = {
    username: this.state.username,
    email: this.state.email,
    adresse_facturation: this.state.adresse_facturation
    };
    console.log("Click");
    this.save(updateUser);
  };
  handleChangeCountry = country => {
    this.setState({ country });
    console.log(`country:`, country);
  };
  
  render() {

    const { country,city,postal,address } = this.state;
    return (
          <div>
            <Navbar></Navbar>
            <br/>
            <br/>
            <br/>
            <button onClick={event => {this.back(event)}}>Go Back</button>
            <br/>
            <br/>
            <h2>Edit Info</h2>
            <div className="container">
              <div className="row">
                <div className="col s8 offset-s2">
                  <form noValidate onSubmit={this.onSubmit}>
                    
                    <div className="input-field col s12">
                      <table>
                        <tr>
                            <td>{this.state.username}</td>
                        </tr>

                      </table>
                    </div>
                    <div className="input-field col s12">
                      <input
                        onChange={this.onChange}
                        value={this.state.email}
                        id="email"
                        type="text"
                      />
                      <label htmlFor="email">{localStorage.getItem("email")}</label>
                    </div>



                    
              <div className="input-field col s12">
                  <table summary className="form-address-table">
                    <tbody>
                      <tr><th> Facturation Address
                      </th></tr>
                      <tr><td colSpan={2}>
                        <span style={{ verticalAlign: 'top' }}>
                          <label htmlFor="select_country">Country</label>
                          <Select id="select_country" 
                            menuPlacement="auto" 
                            placeholder={this.state.adresse_facturation.Country}
                            isSearchable options={options}
                            isSearchable value = {country}
                            onChange={this.handleChangeCountry}>
                          </Select>
                        </span>
                      </td>
                      </tr>
                      <tr>
                        <td colSpan={2}>
                          <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="address">Address</label>
                            <input 
                              id="address" 
                              onChange={this.onChange}
                              value={address} 
                              placeholder={this.state.adresse_facturation.Address}
                              name="address"/>
                          </span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td>
                        
                        <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="city">City</label>
                            <input 
                              id="city" 
                              onChange={this.onChange}
                              value={city} 
                              placeholder={this.state.adresse_facturation.City}
                              name="city"/>

                          </span>
                        </td>
                        <td>
                        <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="postal">Postal Code</label>
                            <input 
                              id="postal" 
                              placeholder={this.state.adresse_facturation.Postal}
                              onChange={this.onChange}
                              value={postal} 
                              name="postal"/>

                          </span>
                        
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>






                    <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                      <button
                        style={{
                          width: "150px",
                          borderRadius: "3px",
                          letterSpacing: "1.5px",
                          marginTop: "1rem"
                        }}
                        type="submit"
                        className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                      >
                        SAVE
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
    );
  
  }
}