import React from 'react';

const PrintData = () =>{

    const username = localStorage.getItem("username");
        const role = localStorage.getItem("role");
        const email = localStorage.getItem("email");
        const date = localStorage.getItem("date");

        return([
            
            <div>
                
                <br/>

                <table>
                    <tr>
                        <td>Username</td>
                        <td>Email</td>
                        <td>Role</td>
                        <td>Date</td>
                    </tr>
                    <tr>
                        <td>{username}</td>
                        <td>{email}</td>
                        <td>{role}</td>
                        <td>{date}</td>
                    </tr>
                </table>
            </div>
       
        ])
    
}
export default PrintData;







        