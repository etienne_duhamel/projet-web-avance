import React from "react";
import UserData from "./User_data.jsx";
import Navbar from "../Navbars/Navbar.jsx";
import edit from './images/edit.jpg';
import API from "../../utils/API";

import './placeholder.css'

export class MyAccount extends React.Component {

  state = {
    isLoading: true,
    items: [],
    error: null
  }

  fetchUsers = () => {
    API.fetchUser()
  .then((response) => {
    return response.json();
  })
  .then(data => {
    this.setState({
      items: data,
      isLoading: false
    });
    //console.log(this.state.items)
  }
  )
  // Catch any errors we hit and update the app
  .catch(error => this.setState({ error, isLoading: false }));
  }

  back = (e) => {
    window.history.back();
  }


  edit_info = () => {
    window.location = "/editinfo";
  };
  
  change_password = () => {
    window.location = "/changepwd";
  };

  componentDidMount() {
    this.fetchUsers();
  }

  render() {
    return (
      
      <div className="Dashboard">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />

        <div className="Navbar">
        <Navbar></Navbar>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        
        <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>My Informations</strong></span></h1>
        <br/>

        <div className="edit">
          <img src={edit} alt="edit" style={{width: '2%', minHeight: '5px'}} />
          <button onClick={this.edit_info}>Edit Info</button>
          <button onClick={this.change_password}>Change Password</button>
        </div>
        <UserData items={this.state.items}/>
        <br/>
        <br/>
        <button onClick={event => {this.back(event)}}>Go Back</button>
      </div>
    
    );
  }
}