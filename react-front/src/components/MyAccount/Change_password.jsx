import React from "react";
import API from "../../utils/API";
import Navbar from "../Navbars/Navbar"

export class ChangePwd extends React.Component {
  constructor() {
    super();
    this.state = {
      password: "",
      password2: "",
      errors: {}
    };
  }
  
  save = async () => {
    const {password, password2} = this.state;
    console.log(password);
    if (!password || password.length === 0 || password !== password2) return alert("Passwords are different!");
    try {
      await API.changepwd({password});//before, await return was stored in const{data}
      window.location="/myaccount";
    } catch (error) {
      console.error(error);
    }
  };
  back = (e) => {
    window.history.back();
  };
 
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const changePwd = {
    password: this.state.password,
    password2: this.state.password2
    };
    console.log(changePwd);
    this.save();
  };
  selectChange = (event) => {
    this.setState({[event.target.id]: this.inputEl.value });
  };
  
  render() {
    const { errors } = this.state;
    return (

      <div>
        <Navbar></Navbar>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <h2>Change you password</h2>
        <br/>
        <br/>
        <button onClick={event => {this.back(event)}}>Go Back</button>
        <br/>
        <br/>

      <div className="container">

      <div className="row">
          <form noValidate onSubmit={this.onSubmit}>
          <div className="input-field col s12">
              <input
                onChange={this.onChange}
                value={this.state.password}
                error={errors.password}
                id="password"
                type="password"
              />
              <label htmlFor="password">Enter New Password</label>
            </div>
            
            <div className="input-field col s12">
              <input
                onChange={this.onChange}
                value={this.state.password2}
                error={errors.password2}
                id="password2"
                type="password"
              />
              <label htmlFor="password2">Confirm New Password</label>
            </div>
            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
              <button
                style={{
                  width: "150px",
                  borderRadius: "3px",
                  letterSpacing: "1.5px",
                  marginTop: "1rem"
                }}
                type="submit"
                className="btn btn-large waves-effect waves-light hoverable blue accent-3"
              >
                SAVE
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  
  );
  }
}