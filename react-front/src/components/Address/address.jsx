
import React from 'react';

export default class Address extends React.Component {
    render = () => {
       
        const { datas } = this.props;
        console.log(datas)
        
        if (!datas) {
           
            return(<h1>Loading...</h1>)
        }
        else {
            console.log("PROPS", datas);
        }
        return([
            <div>
            <tr>
                <td>Country : {datas.Country}</td>
            </tr>
            <tr>
                <td>City : {datas.City}</td>
            </tr>
            <tr>
                <td>Postal Code : {datas.Postal}</td>
            </tr>
            <tr>
                <td>Address : {datas.Address}</td>
            </tr>
        </div>
        ])
    }
}
