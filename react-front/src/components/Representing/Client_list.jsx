import React, { Component } from "react";

import API from "../../utils/API";
import Navbar from "../Navbars/Navbar";
import PrintAllClientList from "./Print_AllClients";



export class ClientList extends Component {

    state = {
        isLoading: true,
        items: [],
        error: null
    }
    
    client_list = () => {
        
        API.client_list()
            .then((response) => {
                return response.json();
            })
            .then(data => {
                
                //console.log(data);
                
                this.setState({
                    items: data,
                    isLoading: false
                });
                
                console.log(this.state.items.data[0]);
                // var rcv = this.state.items.data;

            }
            )
            // Catch any errors we hit and update the app
            .catch(error => this.setState({ error, isLoading: false }));
            
    };

    componentDidMount() {
        this.client_list();
      };

handleClick = (e) => {
    //console.log(e.target.value);
    const newClient = {
        username: localStorage.getItem("username"),
        client: e.target.value

    }
    API.addclient(newClient);
    
    
        window.location.reload()
    
}
    render() {
        var rcv = this.state.items.data;
        console.log("MAIN :",rcv);
        if(!rcv){
            return (
                <div>
                    <Navbar></Navbar> 
                    <br />
                    <br />
                    <br />
                    <h1>Loading...</h1>
                    </div>)
        }
        else{
            console.log("RCV : ",rcv);
        }
        return (
            <div>
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                <Navbar></Navbar> 
                <br />
                <br />
                <br />
                <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>Client List</strong></span></h1>
                <PrintAllClientList datas={rcv} whenClicked={(event)=>{this.handleClick(event)}}></PrintAllClientList>
                
                
               
                
                
            </div>
          );
        
    }
}
