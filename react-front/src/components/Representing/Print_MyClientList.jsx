import React from 'react';
//import API from "../../utils/API";

export default class PrintMyClientList extends React.Component {
   

addclient = () => {
    window.location="/clientlist";
}

back = (e) => {
    window.history.back();
}

render = () =>{
    
    const {datas, whenClicked} = this.props;
    console.log("PROPS", datas);
    if(!datas){

        return(
            
                <h1>Loading....</h1>
        
        )
    }
    else if(datas[0]){
    return(
        <div>
            <br/>
            <br/>
            <button onClick={event => {this.back(event)}}>Go Back</button>
            <br/>
            <br/>
            <table>

                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Creation Date</th>
                    <th>Options</th>
                </tr>


        {datas.map(function(item,index){
            
            
            return ([
            
            <tr id={index+1}>
                <td key={item.username}>{item.username}</td>
                <td key={item.email}>{item.email}</td>
                <td key={item.role}>{item.role}</td>
                <td key={item.createdAt}>{item.createdAt}</td>
                <button type="button" name = "remove" id={index+1} value={item.username} onClick={whenClicked}>Remove Client</button>
                <button type="button" name = "details"id={index+1} value={item._id} onClick={whenClicked}>Client Details</button>
                <button type="button" name = "orders" id={index+1} value={item.username} onClick={whenClicked}>Client Orders</button>
            </tr>
            ])
        })}
            </table>
            </div>
        )
    }
    else {
        return(
            <div>
                <br/>
                <br/>
                <h2>You don't have clients yet, you want to add some?</h2>
                <button onClick={this.addclient}>Add Clients</button>
                <button onClick={event => {this.back(event)}}>Go Back</button>

            </div>
        )
    }
}


}




        