import React, { Component } from "react";

import API from "../../utils/API";
import Navbar from "../Navbars/Navbar";
import PrintMyClientList from "./Print_MyClientList";



export class MyClientList extends Component {

    state = {
        isLoading: true,
        items: [],
        error: null
    }
    
    myclient_list = () => {
        
        API.myclient_list()
            .then((response) => {
                return response.json();
            })
            .then(data => {
                
                //console.log(data);
                
                this.setState({
                    items: data,
                    isLoading: false
                });
                
                console.log(this.state.items.data[0]);
                // var rcv = this.state.items.data;

            }
            )
            // Catch any errors we hit and update the app
            .catch(error => this.setState({ error, isLoading: false }));
            
    };

    componentDidMount() {
        this.myclient_list();
      };

handleClick = (e) => {
    if(e.target.name === "remove"){
        console.log(e.target.value);
        const deletedClient = {
            representant: localStorage.getItem("username"),
            client: e.target.value
        }
        API.deleteClient(deletedClient);
   
        window.location.reload()
    }
    else if(e.target.name === "details"){
        console.log("details");
        window.location ='/userfile?id='+e.target.value
    }
    else{
        console.log("client orders");
        
        window.location = '/clientorders?id='+e.target.value

    }
    

    
}
    render() {
        var rcv = this.state.items.data;
        console.log("MAIN :",rcv);
        if(!rcv){
            return (
                <div>
                    <Navbar></Navbar> 
                    <br />
                    <br />
                    <br />
                    <h1>Loading...</h1>
                    </div>)
        }
        return (
            <div>
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                <Navbar></Navbar> 
                <br />
                <br />
                <br />
                <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>Client List</strong></span></h1>
                <PrintMyClientList datas={rcv} whenClicked={(event)=>{this.handleClick(event)}}></PrintMyClientList>
                
                
               
                
                
            </div>
          );
        
    }
}
