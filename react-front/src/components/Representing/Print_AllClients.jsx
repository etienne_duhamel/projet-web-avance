import React from 'react';


export default class PrintAllClientList extends React.Component {
   

back = (e) => {
    window.location="/";
}
render = () =>{
    
    const {datas, whenClicked} = this.props;
    console.log("PROPS", datas);
    if(!datas){

        return(<h1>Loading...</h1>)
    }
    
    return(
        <div>
            <br/>
            <br/>
            <button onClick={event => {this.back(event)}}>Go Back</button>
            <br/>
            <br/>

            <table>

                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Creation Date</th>
                    <th>Options</th>
                </tr>


        {datas.map(function(item,index){
            
            
            return ([
            
            <tr id={index+1}>
                <td key={item.username}>{item.username}</td>
                <td key={item.email}>{item.email}</td>
                <td key={item.role}>{item.role}</td>
                <td key={item.createdAt}>{item.createdAt}</td>
                <button type="button" id={index+1} value={item.username} onClick={whenClicked}>Add as my Client</button>
            </tr>
            ])
        })}
            </table>
        </div>
        )
        
    }
    

}




        