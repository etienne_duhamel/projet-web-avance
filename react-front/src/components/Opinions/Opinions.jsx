import React from "react";

import Navbar from "../Navbars/Navbar.jsx";

import API from "../../utils/API";

import OpinionList from "./Opinion_data";


export class Opinion_List extends React.Component {

  state = {
    isLoading: true,
    items: [],
    error: null
  }

  fetchOpinion = () => {
    API.fetchOpinions()
  .then((response) => {
    return response.json();
  })
  .then(data => {
    
    this.setState({
      items: data,
      isLoading: false
    });

    const rcv = this.state.items.data;
    console.log(rcv)
    

  }
  )
  // Catch any errors we hit and update the app
  .catch(error => this.setState({ error, isLoading: false }));
  }

  componentDidMount() {
    this.fetchOpinion();
  }

  render() {
    var rcv = this.state.items.data;
    return (
      
      <div className="Dashboard">
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
        <div className="Navbar">
        <Navbar></Navbar>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <h2><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>User's Opinions</strong></span></h2>
        <br/>
        <br/>
        <br/>
        <br/>
        <OpinionList datas={rcv}/>
        

      </div>
    
    );
  }
}
