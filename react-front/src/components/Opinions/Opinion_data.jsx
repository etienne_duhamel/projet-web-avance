import React from 'react';

const  redirection = () => {
    window.history.back();
}

const OpinionList = (datas) =>{

   

    var child= datas.datas;
    if(!child){

        return(<h1>Loading...</h1>)
    }
    return(
        <div>
            <table>

                <tr>
                    <th>Username</th>
                    <th>Opinion</th>
                    
                </tr>

        {child.map(function(item){
            
            return ([
            
            <tr>
                <td key={item.username}>{item.username}</td>
                <td key={item.opinion}>{item.opinion}</td>
                
                
            
            </tr>
            ])
        })}

            </table>
             <button onClick={redirection}>Go Back</button>
            </div>
        )
}

export default OpinionList;
