import React from "react";
import API from "../../utils/API";
import Navbar from "../Navbars/Navbar"

export class SubmitOpinion extends React.Component {
  constructor() {
    super();
    this.state = {
      username: localStorage.getItem("username"),
      opinion: ""
    };
  }
  
  save = async (NewOpinion) => {
    const username = NewOpinion.username;
    const opinion = NewOpinion.opinion;
    if (!opinion || opinion.length === 0) return alert("non mec");
    try {
      await API.giveOpinion({username, opinion}); //before, await return was stored in const{data}
      alert("Opinion submitted! Thank you :)");
      window.location="/";
    } catch (error) {
      console.error(error);
    }
  };
  back = (e) => {
    window.history.back();
  };
 
  onSubmit = e => {
    e.preventDefault();
    const NewOpinion = {
    username: this.state.username,
    opinion: document.getElementById("opinion").value
    };
    console.log(NewOpinion);
    this.save(NewOpinion);
  };
  
  render() {
    
    return (
          <div>
            <Navbar></Navbar>
            <br/>
            <br/>
            <br/>
            <br/>
            <h2>Give my Opinion</h2>
            <div className="container">
              <div className="row">
                <div className="col s8 offset-s2">
                    
              
                  <form id="submitopinion" noValidate onSubmit={this.onSubmit}>
                    
                    <div className="input-field col s12">

                      <textarea 
                        form="submitopinion"
                        name="opinion"
                        id="opinion"
                        cols="20">
            
                      </textarea>
                      <label htmlFor="opinion">Give us your Opinion</label>
                    </div>
                    <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                      <button
                        style={{
                          width: "150px",
                          borderRadius: "3px",
                          letterSpacing: "1.5px",
                          marginTop: "1rem"
                        }}
                        type="submit"
                        className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                      >
                        SEND
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <br/>
            <button onClick={event => {this.back(event)}}>Go Back</button>
          </div>
    );
  
  }
}