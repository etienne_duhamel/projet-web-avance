import React from "react";
import API from "../../utils/API";

const Navbar = () => {


  const disconnect = () => {
    API.logout();
    window.location = "/";
  };

  if(localStorage.getItem("role") === "Admin"){
     return([
        <div>
      <title>Gestion Stocks</title>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <style dangerouslySetInnerHTML={{__html: "\nbody {font-family: \"Lato\", sans-serif}\n.mySlides {display: none}\n" }} />
      {/* Navbar */}
      <div className="w3-top">
        <div className="w3-bar w3-black w3-card">
          <a href="/" className="w3-bar-item w3-button w3-padding-large">HOME</a>
          <a href="/myaccount" className="w3-bar-item w3-button w3-padding-large w3-hide-small">{localStorage.getItem("role")}: {localStorage.getItem("username")}</a>
          <a href="/stock" className="w3-bar-item w3-button w3-padding-large w3-hide-small">ALL ARTICLES</a>
          <a href="/userlist" className="w3-bar-item w3-button w3-padding-large w3-hide-small">ALL USERS</a>
          <a href="/commandlist" className="w3-bar-item w3-button w3-padding-large w3-hide-small">ALL ORDERS</a>
          <a href="/myaccount" className="w3-bar-item w3-button w3-padding-large w3-hide-small w3-right">My Account</a>
          <a href="/#" onClick={disconnect} className="w3-bar-item w3-button w3-padding-large w3-hide-small w3-right">Logout</a>
        </div>
      </div>    
    </div>
  ])
  }
  else if(localStorage.getItem("role") === "Representant"){
      return([
    <div>
      <title>Gestion Stocks</title>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <style dangerouslySetInnerHTML={{__html: "\nbody {font-family: \"Lato\", sans-serif}\n.mySlides {display: none}\n" }} />
      {/* Navbar */}
      <div className="w3-top">
        <div className="w3-bar w3-black w3-card">
          <a href="/" className="w3-bar-item w3-button w3-padding-large">HOME</a>
          <a href="/myaccount" className="w3-bar-item w3-button w3-padding-large w3-hide-small">{localStorage.getItem("role")}: {localStorage.getItem("username")}</a>
          <a href="/addorder" className="w3-bar-item w3-button w3-padding-large">SHIP</a>
          <a href="/myorders" className="w3-bar-item w3-button w3-padding-large">MY ORDERS</a>
          <a href="/clientlist" className="w3-bar-item w3-button w3-padding-large">ADD CLIENT</a>
          <a href="/myclients" className="w3-bar-item w3-button w3-padding-large">MY CLIENTS</a>
          <a href="/myaccount" className="w3-bar-item w3-button w3-padding-large w3-hide-small w3-right">My Account</a>
          <a href="/#" onClick={disconnect} className="w3-bar-item w3-button w3-padding-large w3-hide-small w3-right">Logout</a>
        </div>
      </div>    
    </div>
  ])
  }
  else {
      return([
    <div>
      <title>Gestion Stocks</title>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
      <style dangerouslySetInnerHTML={{__html: "\nbody {font-family: \"Lato\", sans-serif}\n.mySlides {display: none}\n" }} />
      {/* Navbar */}
      <div className="w3-top">
        <div className="w3-bar w3-black w3-card">
          <a href="/" className="w3-bar-item w3-button w3-padding-large">HOME</a>
          <a href="/myaccount" className="w3-bar-item w3-button w3-padding-large w3-hide-small">{localStorage.getItem("role")}: {localStorage.getItem("username")}</a>
          <a href="/addorder" className="w3-bar-item w3-button w3-padding-large w3-hide-small">SHIP</a>
          <a href="/myorders" className="w3-bar-item w3-button w3-padding-large w3-hide-small">MY ORDERS</a>
          <a href="/myaccount" className="w3-bar-item w3-button w3-padding-large w3-hide-small w3-right">My Account</a>
          <a href="/#" onClick={disconnect} className="w3-bar-item w3-button w3-padding-large w3-hide-small w3-right">Logout</a>
        </div>
      </div>    
    </div>
  ])

  }
  
}
export default Navbar;