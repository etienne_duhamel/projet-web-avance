import React from "react";

import christopher from './images/chris.png';
import romain from './images/romain.jpeg';
import etienne from './images/etienne.jpeg';
import yohan from './images/yohan.jpeg';
import map from './images/map.jpg';
import wallpaper from './images/Logo.png';




export class Vitrine extends React.Component {


  
  render() {
    
    return (
      <div>
        <title>Gestion Stocks</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
        <style dangerouslySetInnerHTML={{__html: "\nbody {font-family: \"Lato\", sans-serif}\n.mySlides {display: none}\n" }} />
        {/* Navbar */}
        <div className="w3-top">
          <div className="w3-bar w3-black w3-card">
            <a href="#top" className="w3-bar-item w3-button w3-padding-large">HOME</a>
            <a href="#team" className="w3-bar-item w3-button w3-padding-large w3-hide-small">TEAM</a>
            <a href="#presentation" className="w3-bar-item w3-button w3-padding-large w3-hide-small">PRESENTATION</a>
            <a href="#contact" className="w3-bar-item w3-button w3-padding-large w3-hide-small">CONTACT</a>
            <a href="/login" className="w3-bar-item w3-button w3-padding-large w3-hide-small w3-right">SIGN-IN</a>
          </div>
        </div>
        {/* Image Header */}
        <div className="w3-display-container w3-animate-opacity" id="top">
          <img src={wallpaper} alt="wallpaper" style={{width: '100%', minHeight: '350px', maxHeight: '600px'}} />
        </div>
        {/* Team Container */}
        <div className="w3-container w3-padding-64 w3-center" id="team">
          <h2><span className="w3-xlarge w3-border-teal w3-bottombar">OUR TEAM</span></h2>
          <p>Meet the team - our office rats:</p>
          <div className="w3-row"><br />
            <div className="w3-quarter">
              <img src={christopher} alt="Boss" style={{width: '45%'}} className="w3-circle w3-hover-opacity" />
              <h3>ROQUE Christopher</h3>
              <p>Web Designer</p>
            </div>
            <div className="w3-quarter">
              <img src={romain} alt="Boss" style={{width: '45%'}} className="w3-circle w3-hover-opacity" />
              <h3>VERGNAUD Romain</h3>
              <p>Web Designer</p>
            </div>
            <div className="w3-quarter">
              <img src={etienne} alt="Boss" style={{width: '45%'}} className="w3-circle w3-hover-opacity" />
              <h3>DUHAMEL Etienne</h3>
              <p>Web Designer</p>
            </div>
            <div className="w3-quarter">
              <img src={yohan} alt="Boss" style={{width: '45%'}} className="w3-circle w3-hover-opacity" />
              <h3>CHIRLE Yohan</h3>
              <p>Web Designer</p>
            </div>
          </div>
        </div>
        
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>

        {/* Work Row */}
        <div className="w3-container w3-padding-64 w3-center" id="presentation">
          <h2><span className="w3-xlarge w3-border-teal w3-bottombar">OUR PROJECT</span></h2>
          <p>Etudiants en 4ème année d'ingénieurs à l'INSA CENTRE-VAL DE LOIRE</p>
          <p>Option Sécurité et Technologies Informatiques</p>
          <p>Spécialité Commerce Electronique, Gestion stocks s'incrit dans le cadre d'un projet de Web Avancée</p>
          
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <br/>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3>ROQUE Christopher</h3>
                <h4>Contribution</h4>
                <p></p>
                <p>Controllers Backend</p>
                <p>Migration/Develoment React</p>
                <p>RBAC React</p>
              </div>
            </div>
          </div>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3>VERGNAUD Romain</h3>
                <h4>Contribution</h4>
                <p>Controllers Backend</p>
                <p>Development React</p>
                <p>API React Support</p>
              </div>
            </div>
          </div>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3>DUHAMEL Etienne</h3>
                <h4>Contribution</h4>
                <p>Authentication Backend</p>
                <p>Authorization Backend</p>
                <p>Role-Based Access Backend</p>
              </div>
            </div>
          </div>
          <div className="w3-quarter">
            <div className="w3-card w3-white">
              <div className="w3-container">
                <h3>CHIRLE Yohan</h3>
                <h4>Contribution</h4>
                <p>Backend Views</p>
                <p>Backend Models</p>
                <p>Database Gestion/Communication</p>
              </div>
            </div>
          </div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        {/* Contact Container */}
        <div className="w3-container w3-padding-64 w3-center" id="contact">
          
              <div className="w3-padding-16"><span className="w3-xlarge w3-border-teal w3-bottombar">Contact Us</span></div>
              <h3>Address</h3>
              <p>Swing by for a cup of coffee, or whatever.</p>
              <p><i className="fa fa-map-marker w3-text-teal w3-xlarge" />  Bourges (18000), FR</p>
              <p><i className="fa fa-envelope-o w3-text-teal w3-xlarge" />  christopher.roque@insa-cvl.fr</p>
              <p><i className="fa fa-envelope-o w3-text-teal w3-xlarge" />  romain.vergnaud@insa-cvl.fr</p>
              <p><i className="fa fa-envelope-o w3-text-teal w3-xlarge" />  etienne.duhamel@insa-cvl.fr</p>
              <p><i className="fa fa-envelope-o w3-text-teal w3-xlarge" />  yohan.chirle@insa-cvl.fr</p>
            
        </div>
        {/* Image of location/map */}
        <img src={map} alt="map" className="w3-image w3-greyscale-min" style={{width: '100%'}} />
        {/* Footer */}
        <footer className="w3-container w3-padding-32 w3-theme-d1 w3-center">
          <div style={{position: 'relative', bottom: '25px', zIndex: 1}} className="w3-tooltip w3-right">
            <span className="w3-text w3-padding w3-teal w3-hide-small">Go To Top</span>   
            <a className="w3-button w3-theme" href="#top"><span className="w3-xlarge">
                <i className="fa fa-chevron-circle-up" /></span></a>
          </div>
          <p>Copyright © Gestion Stocks. 2020</p>
        </footer>
      </div>
    );
  }
}
