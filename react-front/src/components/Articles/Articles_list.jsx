import React, { Component } from "react";

import API from "../../utils/API";
import Navbar from "../Navbars/Navbar";
import PrintArticleList from "./Print_articles"

export class ArticlesList extends Component {

    state = {
        isLoading: true,
        items: [],
        error: null
    }
    
    articles_list = () => {
        
        API.fetchArticles()
            .then((response) => {
                return response.json();
            })
            .then(data => {
                
                //console.log(data);
                this.setState({
                    items: data,
                    isLoading: false
                });
                console.log(this.state.items.articles);
                //var rcv = this.state.items.data;
                
                /*
                for (var i = 0; i < rcv.length; i++){
                    console.log(this.state.items.data[i]);
                    
                    
                }
                */
            }
            )
            // Catch any errors we hit and update the app
            .catch(error => this.setState({ error, isLoading: false }));
    }

    componentDidMount() {
        this.articles_list();
      }

    add_article = () => {
        window.location="/addarticle"
    }

    handleClick = (e) => {
        //console.log(e.target.value);
        API.deleteArticle(e.target.value)
        //console.log("INSIDE",e.target.id);
           // var table = document.getElementsByTagName('table')[0];
           // table.deleteRow(e.target.id);
            window.location.reload()
        
    }
      render() {
        var rcv = this.state.items.articles;
        console.log("MAIN :",rcv);
        if(!rcv){
            return (
                <div>
                    <Navbar></Navbar> 
                    <br />
                    <br />
                    <br />
                    <h1>Loading...</h1>
                    </div>)
        }
        return (
            <div>
                <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                <Navbar></Navbar> 
                <br />
                <br />
                <br />
                <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>Articles in Stock</strong></span></h1>

                <br/>
                <br/>
                <button onClick={this.add_article}>Add New Article</button>
                <PrintArticleList datas={rcv} whenClicked={(event)=>{this.handleClick(event)}}></PrintArticleList>
                
                
                
                
               
                
                
            </div>
          );
        
    }
}

//<PrintmyorderList datas={rcv} whenClicked={(event)=>{this.handleClick(event)}}></PrintmyorderList>
//whenClicked={(event)=>{this.handleClick(event)}}
