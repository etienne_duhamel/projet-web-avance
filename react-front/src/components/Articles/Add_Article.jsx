import React from "react";
import API from "../../utils/API";
import Navbar from "../Navbars/Navbar"

export class AddArticle extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "",
      price: "",
      description: ""
    };
  }
  
  save = async (newArticle) => {
    console.log(newArticle);
   
    try {
      await API.addArticle(newArticle);
      window.location="/stock";
    } catch (error) {
      console.error(error);
    }

   
  };
 
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const newArticle = {
    Name: this.state.name,
    Price: this.state.price,
    Description: document.getElementById("description").value
    };
    
    this.save(newArticle);
  };
  selectChange = (event) => {
    this.setState({[event.target.id]: this.inputEl.value });
  };
  
  render() {
    
    return (
          <div>
             <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
            <Navbar></Navbar>
            <br/>
            <br/>
            <br/>
            <br/>
            <h1><span className="w3-xlarge w3-border-teal w3-bottombar"><strong>New Article</strong></span></h1>

            <div className="container">
              <div className="row">
                <div className="col s8 offset-s2">
                  <form noValidate id="newarticle" onSubmit={this.onSubmit}>
                    <div className="input-field col s12">
                      <input
                        onChange={this.onChange}
                        value={this.state.name}
                        id="name"
                        type="text"
                      />
                      <label htmlFor="name">Name</label>
                    </div>
                    <div className="input-field col s12">
                    <textarea 
                        form="newarticle"
                        name="description"
                        id="description"
                        cols="20">
            
                      </textarea>
                      <label htmlFor="description">Description</label>
                    </div>

                    <div className="input-field col s12">
                        <input
                            onChange={this.onChange}
                            value={this.state.price}
                            id="price"
                            type="number"
                        />
                        <label htmlFor="price">Price (€)</label>
                    </div>
                    <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                      <button
                        style={{
                          width: "150px",
                          borderRadius: "3px",
                          letterSpacing: "1.5px",
                          marginTop: "1rem"
                        }}
                        type="submit"
                        className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                      >
                        ADD ARTICLE
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
    );
  
  }
}