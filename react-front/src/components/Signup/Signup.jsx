import React, { Component } from "react";
import { Link } from "react-router-dom";
import API from "../../utils/API";
import Select from "react-select";
import countryList from 'react-select-country-list';

const options = countryList().getData();
const roles = [
  { value: "Admin", label: "Admin"},
  { value: "Representant", label: "Representant"},
  { value: "User", label: "User"},
]


export class Signup extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      password2: "",
      address: "",
      role: "",
      errors: {},
      country: "",
      city: "",
      postal: "",
      checked:""
    };
  }
  send = async () => {
    const { username, email, password, password2, role} = this.state;
    var address = [];
    address.push([this.state.country,this.state.city,this.state.postal,this.state.address]);
    console.log(role.value);
    if (!email || email.length === 0) return;
    if (!password || password.length === 0 || password !== password2) return alert("Passwords are different!");
    try {
      //console.log({ username, email, password, role, address})
      await API.signup({ username, email, password, role, address});//before, await return was stored in const{data}
      alert("Successfully registered, you can login now");
      window.location = "/login";
      
    } catch (error) {
      console.error(error);
    }
  };
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  handleChangeCountry = country => {
    this.setState({ country });
    console.log(`country:`, country);
  };
  handleChangeRole = role => {
    this.setState({ role });
    console.log(`Role:`, role);
  };
  onSubmit = e => {
    e.preventDefault();
    const newUser = {
    username: this.state.username,
    email: this.state.email,
    password: this.state.password,
    role: this.state.role,
    adresse_facturation: this.state.address
    };
    console.log(newUser);
    this.send(newUser);
  };
  selectChange = (event) => {
    this.setState({[event.target.id]: this.inputEl.value });
  };



  render() {
    const { errors, country,role} = this.state;
    return (
      <div className="container">
        <nav className="z-depth-0">
          <div className="nav-wrapper white">
            <Link
              to="/"
              style={{
                fontFamily: "monospace"
              }}
              className="col s5 brand-logo center black-text"
            >
              <i className="material-icons">code</i>
              Gestion Stocks
            </Link>
          </div>
        </nav>
        <div className="row">
          <div className="col s8 offset-s2">
            <Link to="/" className="btn-flat waves-effect">
              <i className="material-icons left">keyboard_backspace</i> Back to
              Home
            </Link>
            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
              <h4>
                <b>Register</b> below
              </h4>
              <p className="grey-text text-darken-1">
                Already have an account? <Link to="/login">Log in</Link>
              </p>
            </div>
            <form noValidate onSubmit={this.onSubmit}>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.username}
                  error={errors.username}
                  id="username"
                  type="text"
                />
                <label htmlFor="username">Username</label>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.email}
                  error={errors.email}
                  id="email"
                  type="email"
                />
                <label htmlFor="email">Email</label>
              </div>

              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.password}
                  error={errors.password}
                  id="password"
                  type="password"
                />
                <label htmlFor="password">Password</label>
              </div>
              <div className="input-field col s12">
                <input
                  onChange={this.onChange}
                  value={this.state.password2}
                  error={errors.password2}
                  id="password2"
                  type="password"
                />
                <label htmlFor="password2">Confirm Password</label>
              </div>


              <div className="input-field col s12">
                <label htmlFor="select_role">Role</label>
                    <Select id="select_role" 
                            menuPlacement="auto" 
                            placeholder="Choose a Role" 
                            isSearchable options={roles}
                            isSearchable value = {role}
                            onChange={this.handleChangeRole}>
                      </Select>
              </div>
              <div className="input-field col s12">
                  <table summary className="form-address-table">
                    <tbody>
                      <tr><th> Facturation Address
                      </th></tr>
                      <tr>
                        <td colSpan={2}>
                          <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="address">Address</label>
                            <input 
                              id="address" 
                              onChange={this.onChange}
                              value={this.state.address} 
                              name="address"/>
                          </span>
                        </td>
                      </tr>
                      
                      <tr>
                        <td>
                        
                        <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="city">City</label>
                            <input 
                              id="city" 
                              onChange={this.onChange}
                              value={this.state.city} 
                              name="city"/>

                          </span>
                        </td>
                        <td>
                        <span style={{ verticalAlign: 'top' }}>
                        <label htmlFor="postal">Postal Code</label>
                            <input 
                              id="postal" 
                              onChange={this.onChange}
                              value={this.state.postal} 
                              name="postal"/>

                          </span>
                        
                        </td>
                      </tr>
                      <tr><td colSpan={2}>
                        <span style={{ verticalAlign: 'top' }}>
                          <label htmlFor="select_country">Country</label>
                          <Select id="select_country" 
                            menuPlacement="auto" 
                            placeholder="Choose a country" 
                            isSearchable options={options}
                            isSearchable value = {country}
                            onChange={this.handleChangeCountry}>
                          </Select>
                        </span>
                      </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                
                


              <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                <button
                  style={{
                    width: "150px",
                    borderRadius: "3px",
                    letterSpacing: "1.5px",
                    marginTop: "1rem"
                  }}
                  type="submit"
                  className="btn btn-large waves-effect waves-light hoverable blue accent-3"
                >
                  Sign up
                </button>
              </div>
            </form>

          </div>
        </div>
      </div>
    );
  }
}
