# Fonctionnalités Authentification et RBAC

## localhost:3000/users/signup

 - Requête **POST** contenant *username, email, password et role*
 - Hashe le mot de passe avec *bcrypt.hash*
 - L'ID de l'utilisateur est utilisé pour créer son token, qui permettra de l'identifier et de déterminer à quelles ressources il aura le droit d'accès
 - Les données reçues sont utilisées pour créer un nouvel utilisateur dans la base de données (*username, email, password, role, token*)

## localhost:3000/users/login 

 - Requête **POST** contenant *email* et *password*
 - La fonction *validatePassword* vérifie que le mot de passe est correct
 - En cas d'authentification réussie, un nouveau token est attribué à l'utilisateur, qui permettra de l'identifier comme *logged in*

## localhost:3000/users/:userId

 - Requête **GET**, **PUT** ou **DELETE**  (nécessite un token valide)
 - **GET** permet de récupérer les informations d'un *user* en passant son ID dans l'url
 - **PUT** (nécessite un role *Admin*) permet de modifier les données d'un utilisateur, en passant son ID dans l'url
 - **DELETE** (nécessite un role *Admin*) permet de supprimer un utilisateur de la base de données

## localhost:3000/users/users

 - Requête **GET** (nécessite un token valide ainsi qu'un role *Admin*)
 - Permet de récupérer tous les utilisateurs ainsi que leurs données

## Fonctionnement global
Lors de chaque requête, on vérifie que le token de l'utilisateur n'a pas été compromis, une fois cette vérification passée, le token est parsé et l'ID de l'utilisateur est récupéré. On vérifie également que le token n'a pas expiré.





