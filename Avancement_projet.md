# Suivi d'avancement du projet

Ci-après les fonctionnalités à implémenter, ainsi que les lignes directrices du projet

## Compte Représentant
Le Représentant doit s'inscrire en renseignant les champs suivants :
 - username
 - email
 - password
 - usertype/role : Représentant *(voir Améliorations)*

Le Représentant sera ensuite rédirigé sur une page d'accueil personnalisée, qui affichera :
 - la liste de ses clients (ainsi que leurs caractéristiques)
 - la possibilité d'ajouter des clients (il faudra renseigner leurs username, email ainsi que taux de remise)
 - la possibilité de supprimer des clients (uniquement en tant que Représentant de ces clients)

***Améliorations*** : avant de pouvoir créer leur compte, les Représentants doivent obtenir une clé de la part de l'admin, afin de prouver qu'ils sont bien Représenants, et de garantir que les simples clients ne peuvent pas créer de compte Représentants, et ainsi avoir accès à certaines ressources

## Compte Client
Le Client doit s'inscrire en renseignant les champs suivants :
 - username
 - email
 - password
 - usertype/role : Client

Le Client sera ensuite rédirigé sur une page d'accueil personnalisée, qui affichera :
 - la liste de ses commandes *(voir Améliorations)*
 - la possibilité de passer une nouvelle commande
 - la possibilité d'annuler une commande
 - la liste de ses Représentants

***Améliorations*** : ajouter la possibilité d'avoir accès aux factures en format .pdf pour les commandes passées

## Compte Admin
L'Admin a un compte unique.

Il aura accès à :
 - la liste de tous les Clients ainsi que les commandes associées
 - la liste de tous les Représentants ainsi que les Clients associés *(voir Améliorations)*
 

***Améliorations*** : ajouter une interface permettant de gérer les demandes de création de compte des Représentants, en leur fournissant une clé

## Résumé des pages/fonctionnalités à implémenter

 - [ ] Liste des commandes passées par un client
 - [ ] Liste des clients gérés par un Représentant
 - [ ] Liste des Représentants
 - [ ] Liste des Clients
 - [ ] Passer une commande
 - [ ] Annuler une commande
 - [ ] Ajouter des clients (par Représentant) et définir leur Taux de remise

