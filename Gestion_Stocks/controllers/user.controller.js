const User = require('../models/user.model');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const { roles } = require('../roles')
 
async function hashPassword(password) {
  console.log("HAhSING...");
 return await bcrypt.hash(password, 12);
}
 
async function validatePassword(plainPassword, hashedPassword) {
 return await bcrypt.compare(plainPassword, hashedPassword)
}
 
exports.signup = async (req, res, next) => {
 try {
  const {username, email, password,role, address} = req.body
  console.log(req.body)
  console.log(role.value);
  console.log(address[0][0].label)
  console.log(address[0][1])
  console.log(address[0][2])
  console.log(address[0][3])
  const hashedPassword = await hashPassword(password);
  const user = new User({username, email, password: hashedPassword, role: role.value || "User",address_facturation: {Country:address[0][0].label,City:address[0][1],Postal:address[0][2],Address:address[0][3]}} );
  const token = jwt.sign({ userId: user._id }, process.env.JWT_KEY, {
   expiresIn: "1d"
  });
  user.token = token;
  await user.save();
  res.json({
   data: user,
   token
  })
 } catch (error) {
  next(error)
 }
}

exports.login = async (req, res, next) => {
  console.log("login");
 try {
  const { email, password } = req.body;
  const user = await User.findOne({ email });
  if (!user) return next(new Error('Email does not exist'));
  const validPassword = await validatePassword(password, user.password);
  if (!validPassword) return next(new Error('Password is not correct'))
  const token = jwt.sign({ userId: user._id }, process.env.JWT_KEY, {
   expiresIn: "1d"
  });
  await User.findByIdAndUpdate(user._id, { token })
  console.log("Sending");
  res.status(200).json({
   data: { email: user.email, role: user.role },
   token
  })
 } catch (error) {
  next(error);
 }
}
 
exports.getUsers = async (req, res, next) => {
  console.log("getUsers");
 const users = await User.find({});
 res.status(200).json({
  data: users
 });
}
 
exports.getUser = (req, res, next) => {
  console.log("getUser");
    User.find({token: req.params.userId}, (error, data) => {
    if (error) {
      return next(error)
    } else {
      console.log("DATA",data)
        res.status(200).json({
            data: data
           });
    }
  })
}
 
exports.userList = (req, res, next) => {
  console.log("UserList");

    User.find({}, function(err, users) {
        var userMap = {};
    
        users.forEach(function(User) {
          userMap[User._id] = User;
        });
        
        res.status(200).json({
            data: users
        });
      })
}

exports.userfile = (req, res, next) => {
  console.log("UserList");

    User.findById(req.params.Id, function(err, user) {
      if(err) {
        console.log(err);
        return res.send(err);
      }
      console.log("COMMANDE",user);
      res.status(200).json({
        data: user
      });
    })
}

exports.updateUser = async (req, res, next) => {
  
  
 try {
  
  const update = {
    email: req.body.email,
    address_facturation: {
      Country:req.body.new_address[0].label,
      City: req.body.new_address[1],
      Postal: req.body.new_address[2],
      Address: req.body.new_address[3]
    }

  }
  
  console.log("Update",update);
  
  await User.findOneAndUpdate({token: req.params.userId}, update);
  const user = await User.find({token: req.params.userId});
  
  res.status(200).json({
   data: user,
  });
 } catch (error) {
  next(error)
 }
}

exports.updateUserbyId = async (req, res, next) => {
  
 
try {

  const role = req.body.role.value

 
 const id = req.params.userId

 
 console.log(role);
 console.log(id);

 await User.findByIdAndUpdate({_id: id}, {role: role});
 const user = await User.findById({_id: id});
 
 res.status(200).json({
  data: user,
 });
 
} catch (error) {
 next(error)
}
}

exports.changepwd = async (req, res, next) => {
  
  try {
    
    console.log(req.params.userId)
    const new_password = req.body.password;
    console.log("PASSWORD",new_password);
    
    const hashedPassword = await hashPassword(new_password);
      
      let user =await User.findOneAndUpdate({token: req.params.userId}, {password: hashedPassword},{new: true});
     
        
        console.log(user);
       
        res.status(200).json({
          data: user,
         });
      
     
    } catch (error) {
      console.log("ERROR");
      next(error)
     }
  
    }
    
  
 



exports.deleteUser = async (req, res, next) => {
  
 try {
  const userId = req.params.userId;
  console.log("Delete User : ", userId);
  await User.findOneAndDelete({username: userId});
  res.status(200).json({
   message: 'User has been deleted'
  });
 } catch (error) {
  next(error)
 }
}