
const mongoose = require('mongoose')
const Representing = require('../models/representing.model');
const User = require('../models/user.model');



exports.addClient = function (req, res, next) {

    

    User.findOne(({ username: req.body.username }), function (err, rep) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        User.findOne(({ username: req.body.client }), function (err2, client) {
            if (err2) {
                console.log(err2);
                return res.send(err2);
            }
            var bound = Representing.findOneAndUpdate({ representant: rep }, { $addToSet: { clients: client } }, { new: true, upsert: true })
            bound.exec((err, data) => {
                if (err) {
                    console.log(err);
                    return res.send(err);
                }
                else {
                    
                    console.log(data);
                    res.status(200).json({
                        message: 'Client added'
                    })
                }
            })
        });
    });

  
}


exports.myClients = async (req, res, next) => {
    console.log(req.params.userId);
    await Representing.find({ "representant.username": req.params.userId }, function (err, data) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        else if (data[0]) {

            console.log("IF2", data[0].clients);
            res.status(200).json({
                data: data[0].clients
            })
        }
        else {
            console.log("The representant " + req.params.userId + " has no clients")
            res.status(300).json({
                data: data
            })
        }
    })

}


exports.allClients = async (req, res, next) => {
    
    var clientMap = [];
    
    let query = User.find({ "role": "User" })
    query.exec(async (err, users) => {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        else {
            

            await Representing.find({ "representant.username": req.params.userId }, function (err, data) {
                if (err) {
                    console.log(err);
                    return res.send(err);
                }
                else if (data[0]) {
                    
                    for (var j = 0; j < users.length; j++) {
                        
                        for (var i = 0; i < data[0].clients.length; i++) {
                            

                            if (data[0].clients[i].username === users[j].username) {
                                
                                break;
                            }
                            if (data[0].clients.length === i + 1) {
                                
                                clientMap.push(users[j]);
                            }
                        }
                       
                    }


                }

                else {
                    for (var j = 0; j < users.length; j++) {
                        clientMap.push(users[j]);
                    }

                }
                
                res.status(200).json({
                    data: clientMap
                })
            })


        }
    })
}

exports.deleteClient = async (req, res, next) => {
    var rep = req.body.representant
    var client = req.body.client

    var del = Representing.findOneAndUpdate({ "representant.username": rep }, { $pull: { clients: { username: client } } }, { new: true }, function (err, data) {
        if (err) {
            console.log(err);
            return res.send(err);
        }
        else {
            
            if (data.clients.length === 0) {
                
                Representing.remove({ "representant.username": rep }, function (err3, data2) {
                    
                    res.status(200).json({
                        message: 'Client removed'
                    })
                });


            }
            else {
                res.status(200).json({
                    message: 'Client removed'
                })
            }
        }

    })

  


}