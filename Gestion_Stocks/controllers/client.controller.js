const Client = require('../models/client.model');

exports.addClient = function (req, res, next) {
    let client = new Client(
        {
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            taux_remise: req.body.taux_remise
        }
    );
    client.save(function (err) {
        if (err) {
            return res.send(err);
        }
        res.redirect('/add-client');
    })
}

exports.getUser = (req, res, next) => {
    User.find({token: req.params.userId}, (error, data) => {
    if (error) {
      return next(error)
    } else {
        res.status(200).json({
            data: data
           });
    }
  })
}

exports.client_list = (req, res, next) => {
    

    User.find({}, function(err, users) {
        var userMap = {};
    
        users.forEach(function(User) {
          userMap[User._id] = User;
        });
        
        res.status(200).json({
            data: users
        });
      })

    }
