
const Article = require('../models/article.model');

exports.article_list = function (req, res) {

    Article.find({}, function(err, articles) {
        var articleMap = {};
    
        articles.forEach(function(article) {
          articleMap[article._id] = article;

        });
        console.log("Returning articles");
        return res.status(200).json({articles: articles});
      });
}

exports.delete_article = function (req, res) {

  var id = req.params.Id
    console.log(id)
    
    Article.findByIdAndRemove(id, function (err) {
                
      });
    

}

exports.addArticle = async (req, res, next) => {
  try {
   const {Name, Price, Description} = req.body
   console.log(Name);
   console.log(Price);
   console.log(Description);
   const articles = new Article({Name: Name, Prix: Price, Description: Description});
   await articles.save();
   res.json({
    data: articles
   })
  } catch (error) {
   next(error)
  }
  
 }