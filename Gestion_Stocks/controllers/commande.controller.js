const Commande = require('../models/commande.model');
const Article = require('../models/article.model');
const User = require('../models/user.model');

exports.addCommande = function(req, res, next) {
  var commandeUser = [];
  var articleMap = {};
  
console.log("Add Commande");
  Article.countDocuments(function (err, count) {

  Article.find({}, async(err, articles) =>{
    let j=0;
  
    articles.forEach(function(article) {
      articleMap[j] = article;
      j++;
    });
    console.log("COUNT :", req.body.rows.length);
    for (var k=0; k < req.body.rows.length ; k++){
      for(var i=0;i<count;i++){

        
        if(articleMap[i].Name === req.body.rows[k].texte){
          commandeUser.push([req.body.rows[k].texte,req.body.rows[k].price,req.body.rows[k].quantity,req.body.rows[k].price*req.body.rows[k].quantity]);
        }
    }
  }

 
  const user = await User.findOne({username:req.body.username});
  const {shipping_address} = req.body
  var cmd = new Commande({
    
  
    user: user,
    shipping_address: {Country:shipping_address[0][0].label,City:shipping_address[0][1],Postal:shipping_address[0][2],Address:shipping_address[0][3]},
    
    total: req.body.total,
    date_commande: req.body.date_commande,
    commande:commandeUser
  });


  console.log(cmd)


  cmd.save(function(err) {
    if (err) {
      return res.send(err);
    }
    else{
      res.status(200).json({
        message: "Order added"
      });
    }
  });
})

})
};

exports.deleteCommande = function (req, res, next) {
    var id = req.params.Id
    console.log(id)
    Commande.findByIdAndRemove(id, function (err) {
              
      });
    
}

exports.commande_list = function (req, res) {

    Commande.find({}, function(err, commandes) {
        var commandeMap = {};
    
        commandes.forEach(function(commande) {
          commandeMap[commande._id] = commande;
        });
        res.status(200).json({
          data: commandes
        });
      })
}


exports.commande_list_by_user = function (req, res) {

  console.log(req.params.userId)
  Commande.find({"user.username": req.params.userId}, function(err, commandes) {
      if(err) {
        console.log(err);
        return res.send(err);
      }
      console.log("COMMANDES",commandes);
      res.status(200).json({
        data: commandes
      });
    })
}

exports.commande_detail_by_id = function (req, res) {

  console.log(req.params.Id)
  Commande.findById(req.params.Id, function(err, commandes) {
      if(err) {
        console.log(err);
        return res.send(err);
      }
      console.log("COMMANDE",commandes);
      res.status(200).json({
        data: commandes
      });
    })
}