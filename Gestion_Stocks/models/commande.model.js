const mongoose = require('mongoose');
const Schema = mongoose.Schema;

Commande_Schema = new Schema ({
    
    user: {
        type: Schema.Types.Mixed
    },
    
    shipping_address: {
        type: Schema.Types.Mixed
    },
    
    date_commande: {type: Date, default: Date.now()},
    commande: [{
       
        type: Schema.Types.Mixed
         }],
    total: {type: Number}

});

//Exporter le modèle
module.exports = mongoose.model('Commande', Commande_Schema, 'commandes');