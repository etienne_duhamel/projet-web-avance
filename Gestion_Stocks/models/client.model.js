const mongoose = require('mongoose');
const Schema = mongoose.Schema;

Client_Schema = new Schema ({
    firstName: {type: String, trim: true, lowercase: true},
    lastName: {type: String, trim: true, lowercase: true},
    taux_remise: {type: Number, default: 0},
});

//Exporter le modèle
module.exports = mongoose.model('Client', Client_Schema,'clients');
