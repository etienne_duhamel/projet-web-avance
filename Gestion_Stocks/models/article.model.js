const mongoose = require('mongoose');
const Schema = mongoose.Schema;

Article_Schema = new Schema ({
    Name: {type: String},
    Prix: {type: Number},
    Description: {type: String}
});

//Exporter le modèle
module.exports = mongoose.model('Article', Article_Schema,'articles');
