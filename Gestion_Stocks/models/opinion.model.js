const mongoose = require('mongoose');
const Schema = mongoose.Schema;

Opinion_Schema = new Schema ({
    
    username: {type: String},
    opinion: {type: String}
});

//Exporter le modèle
module.exports = mongoose.model('Opinion', Opinion_Schema,'opinions');
