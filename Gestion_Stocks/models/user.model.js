const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
var Schema = mongoose.Schema

const userSchema = Schema({
    username: {
        type: String,
        required: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        lowercase: true,
        validate: value => {
            if (!validator.isEmail(value)) {
                throw new Error({error: 'Invalid Email address'})
            }
        }
    },
    password: {
        type: String,
        required: true
    },
    
    token: {
            type: String
        }
    ,
    role: {
        type: String,
        default: 'User',
        enum: ["User", "Representant", "Admin"]
    },
    createdAt:{
        type: Date,
        default: Date.now
    },
    address_facturation:{
        type: Schema.Types.Mixed
    },

})

const User = mongoose.model('User', userSchema, 'users')

module.exports = User