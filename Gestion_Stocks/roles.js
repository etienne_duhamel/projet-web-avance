const AccessControl = require("accesscontrol");
const ac = new AccessControl();
 
exports.roles = (function() {
ac.grant("User")
 .readOwn("profile")
 .updateOwn("profile")
 
ac.grant("Representant")
 .extend("User")

 
ac.grant("Admin")
 .extend("User")
 .extend("Representant")
 .readAny("profile")
 .updateAny("profile")
 .deleteAny("profile")
 
return ac;
})();