const express = require('express');
const router = express.Router();

const client_controller = require('../controllers/client.controller');

router.get('/', function (req, res) {
    res.render('add-client', { title: 'New Client' });
});

router.get('/client-list', client_controller.client_list);


router.post('/', client_controller.addClient);

module.exports = router; 
