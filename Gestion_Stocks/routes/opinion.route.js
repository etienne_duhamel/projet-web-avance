const express = require('express');
const Opinion = require('../models/opinion.model');
const OpinionController = require('../controllers/opinion.controller');

const router = express.Router();


router.post('/giveopinion', OpinionController.submitOpinion);
router.get('/opinionlist', OpinionController.OpinionList);


module.exports = router; 