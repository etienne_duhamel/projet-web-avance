const express = require("express");
const router = express.Router();

const commande_controller = require("../controllers/commande.controller");

router.get('/commande-list', commande_controller.commande_list);

router.post('/delete/:Id', commande_controller.deleteCommande);

router.get('/details/:Id', commande_controller.commande_detail_by_id);

router.get('/myorders/:userId', commande_controller.commande_list_by_user);

router.post("/addcommand", commande_controller.addCommande);



module.exports = router;
