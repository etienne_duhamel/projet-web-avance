
const express = require('express');
const User = require('../models/user.model');
const auth = require('../middleware/auth');
const userController = require('../controllers/user.controller')

const router = express.Router();


const representing_controller = require("../controllers/representing.controller");

router.get('/myClients/:userId', representing_controller.myClients);

router.get('/allClients/:userId', representing_controller.allClients);

router.post('/addClient', representing_controller.addClient);

router.post('/deleteClient/', representing_controller.deleteClient);

module.exports = router;