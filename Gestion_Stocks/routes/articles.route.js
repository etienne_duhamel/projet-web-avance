const express = require("express");
const router = express.Router();

const articles_controller = require("../controllers/articles.controller");

router.get("/", articles_controller.article_list);

router.post('/delete/:Id', articles_controller.delete_article);

router.post('/newarticle', articles_controller.addArticle);

module.exports = router;
