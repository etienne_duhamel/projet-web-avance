
const express = require('express');
const User = require('../models/user.model');
const auth = require('../middleware/auth');
const userController = require('../controllers/user.controller')

const router = express.Router();



router.post('/signup', userController.signup);
 
router.post('/login', userController.login);
 
router.get('/user/:userId', userController.getUser);

router.get('/userlist', userController.userList);

router.get('/userfile/:Id', userController.userfile);
 
router.get('/users', auth.allowIfLoggedin, auth.grantAccess('readAny', 'profile'), userController.getUsers);
 
router.put('/update/:userId', userController.updateUser);

router.put('/updateid/:userId', userController.updateUserbyId);

router.post('/changepwd/:userId', userController.changepwd);
 
router.post('/delete/:userId', userController.deleteUser);

module.exports = router;