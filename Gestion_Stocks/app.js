var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const User = require('./models/user.model')




let gestion_db_url = 'mongodb+srv://root:toor@projet-ce-2tgau.azure.mongodb.net/Gestion_Stocks?retryWrites=true&w=majority';
let mongoDB = process.env.MONGODB_URI || gestion_db_url;
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false});
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error',console.error.bind(console,'MongoDB connection error:'));
db.once('open', function (){
  console.log("Connexion to database OK"); 
});


var app = express();

var cors = require('cors');
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(async (req, res, next) => {
   if (req.headers["x-access-token"]) {
  const token = req.headers["x-access-token"];
  const { userId, exp } = await jwt.verify(token, process.env.JWT_KEY);
  // Check if token has expired
  if (exp < Date.now().valueOf() / 1000) { 
   return res.status(401).json({ error: "JWT token has expired, please login to obtain a new one" });
  } 
  res.locals.loggedInUser = await User.findById(userId); next(); 
 } else { 
  next(); 
 } 
});

var indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const commandeRoute = require('./routes/add-commande.route');
const clientRoute = require('./routes/add-client.route');
const loginRoute = require('./routes/login.route');
const articles = require ('./routes/articles.route');
const opinionRoute = require ('./routes/opinion.route');
const representing = require ('./routes/representing.route');
app.use('/', indexRouter);
app.use('/addOrder', commandeRoute);
app.use('/add-client', clientRoute);
app.use('/users', usersRouter);
app.use('/login', loginRoute);
app.use('/signup', loginRoute);
app.use('/articles',articles);
app.use('/opinion', opinionRoute);
app.use('/representing', representing);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
